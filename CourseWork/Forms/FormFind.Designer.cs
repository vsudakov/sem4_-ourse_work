﻿namespace CourseWork.Forms
{
    partial class FormFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFind));
            this.TB_Key = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DGV_Search = new System.Windows.Forms.DataGridView();
            this.B_Go = new System.Windows.Forms.Button();
            this.B_Cancel = new System.Windows.Forms.Button();
            this.HP_FormFind = new System.Windows.Forms.HelpProvider();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.B_Search = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Search)).BeginInit();
            this.SuspendLayout();
            // 
            // TB_Key
            // 
            this.HP_FormFind.SetHelpKeyword(this.TB_Key, "SearchRecord.htm");
            this.HP_FormFind.SetHelpNavigator(this.TB_Key, System.Windows.Forms.HelpNavigator.Topic);
            this.TB_Key.Location = new System.Drawing.Point(210, 9);
            this.TB_Key.Name = "TB_Key";
            this.HP_FormFind.SetShowHelp(this.TB_Key, false);
            this.TB_Key.Size = new System.Drawing.Size(391, 21);
            this.TB_Key.TabIndex = 0;
            this.toolTip1.SetToolTip(this.TB_Key, "Ключ для поиска \"Сорт овоща\".");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ключевое слово для поиска:";
            // 
            // DGV_Search
            // 
            this.DGV_Search.AllowUserToAddRows = false;
            this.DGV_Search.AllowUserToResizeColumns = false;
            this.DGV_Search.AllowUserToResizeRows = false;
            this.DGV_Search.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGV_Search.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DGV_Search.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV_Search.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV_Search.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Search.EnableHeadersVisualStyles = false;
            this.DGV_Search.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.DGV_Search.Location = new System.Drawing.Point(0, 37);
            this.DGV_Search.MultiSelect = false;
            this.DGV_Search.Name = "DGV_Search";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV_Search.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGV_Search.RowHeadersVisible = false;
            this.DGV_Search.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGV_Search.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV_Search.Size = new System.Drawing.Size(667, 178);
            this.DGV_Search.TabIndex = 4;
            this.DGV_Search.Visible = false;
            // 
            // B_Go
            // 
            this.B_Go.Location = new System.Drawing.Point(210, 221);
            this.B_Go.Name = "B_Go";
            this.B_Go.Size = new System.Drawing.Size(138, 23);
            this.B_Go.TabIndex = 5;
            this.B_Go.Text = "Перейти к записи";
            this.toolTip1.SetToolTip(this.B_Go, "Закрыть окно поиска и выделить в таблице выбранную запись.");
            this.B_Go.UseVisualStyleBackColor = true;
            this.B_Go.Visible = false;
            this.B_Go.Click += new System.EventHandler(this.B_Go_Click);
            // 
            // B_Cancel
            // 
            this.B_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.B_Cancel.Location = new System.Drawing.Point(367, 221);
            this.B_Cancel.Name = "B_Cancel";
            this.B_Cancel.Size = new System.Drawing.Size(75, 23);
            this.B_Cancel.TabIndex = 6;
            this.B_Cancel.Text = "Отмена";
            this.B_Cancel.UseVisualStyleBackColor = true;
            this.B_Cancel.Visible = false;
            this.B_Cancel.Click += new System.EventHandler(this.B_Cancel_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // B_Search
            // 
            this.B_Search.Image = global::CourseWork.Properties.Resources.magnifier;
            this.B_Search.Location = new System.Drawing.Point(598, 8);
            this.B_Search.Name = "B_Search";
            this.B_Search.Size = new System.Drawing.Size(23, 23);
            this.B_Search.TabIndex = 2;
            this.toolTip1.SetToolTip(this.B_Search, "Нажмите, чтобы найти запись.");
            this.B_Search.UseVisualStyleBackColor = true;
            this.B_Search.Click += new System.EventHandler(this.B_Search_Click);
            // 
            // FormFind
            // 
            this.AcceptButton = this.B_Search;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.CancelButton = this.B_Cancel;
            this.ClientSize = new System.Drawing.Size(667, 36);
            this.Controls.Add(this.B_Cancel);
            this.Controls.Add(this.B_Go);
            this.Controls.Add(this.DGV_Search);
            this.Controls.Add(this.B_Search);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TB_Key);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HP_FormFind.SetHelpKeyword(this, "SearchRecord.htm");
            this.HP_FormFind.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.HP_FormFind.SetHelpString(this, "");
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFind";
            this.HP_FormFind.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поиск . . .";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Search)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TB_Key;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button B_Search;
        private System.Windows.Forms.DataGridView DGV_Search;
        private System.Windows.Forms.Button B_Go;
        private System.Windows.Forms.Button B_Cancel;
        private System.Windows.Forms.HelpProvider HP_FormFind;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}