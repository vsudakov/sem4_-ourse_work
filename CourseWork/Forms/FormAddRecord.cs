﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CourseWorkLibrary;

namespace CourseWork.Forms
{
    /// <summary>
    /// Форма "Добавление записей".
    /// </summary>
    public partial class FormAddRecord : Form
    {
        /// <summary>
        /// Ссылка на родительскую форму.
        /// </summary>
        FormMain parentForm;

        /// <summary>
        /// Конструктор формы.
        /// </summary>
        /// <param name="parentForm">Ссылка на родительскую форму.</param>
        public FormAddRecord(FormMain parentForm)
        {
            this.parentForm = parentForm;

            InitializeComponent();

            this.HP_FormAddRecord.HelpNamespace = this.parentForm.HP_FormMain.HelpNamespace;

            // Дополнительная инициализация DGV_Add:
            this.DGV_Add.ColumnCount = 5;
            this.DGV_Add.Columns[0].HeaderText = "Вид овоща";
            this.DGV_Add.Columns[1].HeaderText = "Сорт";
            this.DGV_Add.Columns[2].HeaderText = "Номинальная урожайность (кг/м2)";
            this.DGV_Add.Columns[3].HeaderText = "Дата посадки";
            this.DGV_Add.Columns[4].HeaderText = "Дата уборки";

            this.DGV_Add.RowCount = 1;
            this.DGV_Add[0, 0].ToolTipText = "Здесь может быть одно из значений:\nКартофель|Помидор|Огурец|Перец|Редис|Салат\n(это поле можно оставить пустым)";
            this.DGV_Add[1, 0].ToolTipText = "Не оставляйте это поле пустым.";
            this.DGV_Add[2, 0].ToolTipText = "Целое число либо десятичная дробь.\n(это поле можно оставить пустым)";
            this.DGV_Add[3, 0].ToolTipText = "(это поле можно оставить пустым)";
            this.DGV_Add[4, 0].ToolTipText = "(это поле можно оставить пустым)";
        }

        /// <summary>
        /// Нажатие кнопки "Добавить".
        /// </summary>
        private void B_Add_Click(object sender, EventArgs e)
        {
            bool isWasMistakes = false;

            // Сбросить все цвета в ячейках
            for (int i = 0; i < DGV_Add.RowCount - 1; i++)
                for (int j = 0; j < DGV_Add.ColumnCount; j++)
                    DGV_Add[j, i].Style.BackColor = Color.White;

            // Проверить все вводимые данные на корректность, и если таковые есть, изменить цвет ячеек соответствующих значений:
            for (int i = 0; i < this.DGV_Add.RowCount - 1; i++)
            {
                if (!Verify.Type((string)DGV_Add[0, i].Value))
                {
                    isWasMistakes = true;
                    DGV_Add[0, i].Style.BackColor = Color.LightPink;
                }

                if (!Verify.Sort((string)DGV_Add[1, i].Value))
                {
                    isWasMistakes = true;
                    DGV_Add[1, i].Style.BackColor = Color.LightPink;
                }

                if (!Verify.NominalProlificness((string)DGV_Add[2, i].Value))
                {
                    isWasMistakes = true;
                    DGV_Add[2, i].Style.BackColor = Color.LightPink;
                }

                if (!Verify.PlantingDate((string)DGV_Add[3, i].Value))
                {
                    isWasMistakes = true;
                    DGV_Add[3, i].Style.BackColor = Color.LightPink;
                }

                if (!Verify.HarvestingDate((string)DGV_Add[4, i].Value))
                {
                    isWasMistakes = true;
                    DGV_Add[4, i].Style.BackColor = Color.LightPink;
                }
            }

            if (isWasMistakes) // есть ли ошибки в введённых данных?
            {
                DGV_Add.ClearSelection();
                MessageBox.Show("Введённые данные содержат ошибки! (отмечены цветом)\nПожалуйста, исправьте их и повторите снова.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            else
            {
                for (int i = 0; i < DGV_Add.RowCount - 1; i++)
                {
                    Vegetable v = new Vegetable(SaveConvert.ToTypeVegetable((string)DGV_Add[0, i].Value), (string)DGV_Add[1, i].Value, SaveConvert.ToPositiveDouble((string)DGV_Add[2, i].Value), (string)DGV_Add[3, i].Value, (string)DGV_Add[4, i].Value);
                    BinaryTreeHelper.Add(this.parentForm.tree, v);
                }
                BinaryTreeHelper.Copy(this.parentForm.tree, this.parentForm.DGV_Main);
                this.B_Cancel.PerformClick();
            }
        }

        /// <summary>
        /// Нажатие кнопки "Отмена".
        /// </summary>
        private void B_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Добавление строки в DataGridView.
        /// </summary>
        private void DGV_Add_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (DGV_Add.ColumnCount == 5)
            {
                this.DGV_Add[0, DGV_Add.RowCount - 1].ToolTipText = "Здесь может быть одно из значений:\nКартофель|Помидор|Огурец|Перец|Редис|Салат\n(это поле можно оставить пустым)";
                this.DGV_Add[1, DGV_Add.RowCount - 1].ToolTipText = "Не оставляйте это поле пустым.";
                this.DGV_Add[2, DGV_Add.RowCount - 1].ToolTipText = "Целое число либо десятичная дробь.\n(это поле можно оставить пустым)";
                this.DGV_Add[3, DGV_Add.RowCount - 1].ToolTipText = "(это поле можно оставить пустым)";
                this.DGV_Add[4, DGV_Add.RowCount - 1].ToolTipText = "(это поле можно оставить пустым)";
            }
        }
    }
}
