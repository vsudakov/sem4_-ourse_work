﻿namespace CourseWork.Forms
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.MS_Main = new System.Windows.Forms.MenuStrip();
            this.TSMI_File = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_New = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Open = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Save = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_CloseFile = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_AddRecord = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.TSMI_EditRecord = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.TSMI_DeleteRecord = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_DeleteAllRecords = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Search = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_FastSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_FullSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Additionally = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Tree = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_MaxElement = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_SumAll = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Helping = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_AboutProgram = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_AboutAuthor = new System.Windows.Forms.ToolStripMenuItem();
            this.TS_Main = new System.Windows.Forms.ToolStrip();
            this.TSB_New = new System.Windows.Forms.ToolStripButton();
            this.TSB_Open = new System.Windows.Forms.ToolStripButton();
            this.TSB_Save = new System.Windows.Forms.ToolStripButton();
            this.TSB_CloseFile = new System.Windows.Forms.ToolStripButton();
            this.TSB_Exit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.TSB_AddRecord = new System.Windows.Forms.ToolStripButton();
            this.TSB_EditRecord = new System.Windows.Forms.ToolStripButton();
            this.TSB_DeleteRecord = new System.Windows.Forms.ToolStripButton();
            this.TSB_DeleteAllRecords = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.TSB_FullSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.TSB_Tree = new System.Windows.Forms.ToolStripButton();
            this.TSB_MaxElement = new System.Windows.Forms.ToolStripButton();
            this.TSB_SumAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.TSB_Help = new System.Windows.Forms.ToolStripButton();
            this.TSB_AboutProgram = new System.Windows.Forms.ToolStripButton();
            this.TSB_AboutAuthor = new System.Windows.Forms.ToolStripButton();
            this.DGV_Main = new System.Windows.Forms.DataGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.TS_FastSearch = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.TSTB_FastSearch = new System.Windows.Forms.ToolStripTextBox();
            this.TSB_FastSearch = new System.Windows.Forms.ToolStripButton();
            this.HP_FormMain = new System.Windows.Forms.HelpProvider();
            this.MS_Main.SuspendLayout();
            this.TS_Main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Main)).BeginInit();
            this.TS_FastSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // MS_Main
            // 
            this.MS_Main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.MS_Main.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MS_Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_File,
            this.TSMI_Edit,
            this.TSMI_Search,
            this.TSMI_Additionally,
            this.TSMI_Helping});
            this.MS_Main.Location = new System.Drawing.Point(0, 0);
            this.MS_Main.Name = "MS_Main";
            this.MS_Main.Size = new System.Drawing.Size(667, 24);
            this.MS_Main.TabIndex = 0;
            this.MS_Main.Text = "Панель-меню";
            // 
            // TSMI_File
            // 
            this.TSMI_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_New,
            this.TSMI_Open,
            this.TSMI_Save,
            this.TSMI_CloseFile,
            this.TSMI_Exit});
            this.TSMI_File.Name = "TSMI_File";
            this.TSMI_File.Size = new System.Drawing.Size(49, 20);
            this.TSMI_File.Text = "Файл";
            // 
            // TSMI_New
            // 
            this.TSMI_New.Image = global::CourseWork.Properties.Resources.page;
            this.TSMI_New.Name = "TSMI_New";
            this.TSMI_New.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.TSMI_New.Size = new System.Drawing.Size(176, 22);
            this.TSMI_New.Text = "Новый";
            this.TSMI_New.Click += new System.EventHandler(this.TSMI_New_Click);
            // 
            // TSMI_Open
            // 
            this.TSMI_Open.Image = global::CourseWork.Properties.Resources.book_open;
            this.TSMI_Open.Name = "TSMI_Open";
            this.TSMI_Open.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.TSMI_Open.Size = new System.Drawing.Size(176, 22);
            this.TSMI_Open.Text = "Открыть";
            this.TSMI_Open.Click += new System.EventHandler(this.TSMI_Open_Click);
            // 
            // TSMI_Save
            // 
            this.TSMI_Save.Enabled = false;
            this.TSMI_Save.Image = global::CourseWork.Properties.Resources.page_save;
            this.TSMI_Save.Name = "TSMI_Save";
            this.TSMI_Save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.TSMI_Save.Size = new System.Drawing.Size(176, 22);
            this.TSMI_Save.Text = "Сохранить";
            this.TSMI_Save.Click += new System.EventHandler(this.TSMI_Save_Click);
            this.TSMI_Save.EnabledChanged += new System.EventHandler(this.TSMI_Save_EnabledChanged);
            // 
            // TSMI_CloseFile
            // 
            this.TSMI_CloseFile.Enabled = false;
            this.TSMI_CloseFile.Image = global::CourseWork.Properties.Resources.page_red;
            this.TSMI_CloseFile.Name = "TSMI_CloseFile";
            this.TSMI_CloseFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.TSMI_CloseFile.Size = new System.Drawing.Size(176, 22);
            this.TSMI_CloseFile.Text = "Закрыть";
            this.TSMI_CloseFile.Click += new System.EventHandler(this.TSMI_CloseFile_Click);
            this.TSMI_CloseFile.EnabledChanged += new System.EventHandler(this.TSMI_CloseFile_EnabledChanged);
            // 
            // TSMI_Exit
            // 
            this.TSMI_Exit.Image = global::CourseWork.Properties.Resources.cancel;
            this.TSMI_Exit.Name = "TSMI_Exit";
            this.TSMI_Exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.TSMI_Exit.Size = new System.Drawing.Size(176, 22);
            this.TSMI_Exit.Text = "Выход";
            this.TSMI_Exit.Click += new System.EventHandler(this.TSMI_Exit_Click);
            // 
            // TSMI_Edit
            // 
            this.TSMI_Edit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_AddRecord,
            this.toolStripSeparator5,
            this.TSMI_EditRecord,
            this.toolStripSeparator3,
            this.TSMI_DeleteRecord,
            this.TSMI_DeleteAllRecords});
            this.TSMI_Edit.Enabled = false;
            this.TSMI_Edit.Name = "TSMI_Edit";
            this.TSMI_Edit.Size = new System.Drawing.Size(61, 20);
            this.TSMI_Edit.Text = "Правка";
            this.TSMI_Edit.EnabledChanged += new System.EventHandler(this.TSMI_Edit_EnabledChanged);
            // 
            // TSMI_AddRecord
            // 
            this.TSMI_AddRecord.Enabled = false;
            this.TSMI_AddRecord.Image = global::CourseWork.Properties.Resources.database_add;
            this.TSMI_AddRecord.Name = "TSMI_AddRecord";
            this.TSMI_AddRecord.ShortcutKeys = System.Windows.Forms.Keys.Insert;
            this.TSMI_AddRecord.Size = new System.Drawing.Size(215, 22);
            this.TSMI_AddRecord.Text = "Добавить записи";
            this.TSMI_AddRecord.Click += new System.EventHandler(this.TSMI_AddRecord_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(212, 6);
            // 
            // TSMI_EditRecord
            // 
            this.TSMI_EditRecord.Enabled = false;
            this.TSMI_EditRecord.Image = global::CourseWork.Properties.Resources.database_edit;
            this.TSMI_EditRecord.Name = "TSMI_EditRecord";
            this.TSMI_EditRecord.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.TSMI_EditRecord.Size = new System.Drawing.Size(215, 22);
            this.TSMI_EditRecord.Text = "Изменить запись";
            this.TSMI_EditRecord.Click += new System.EventHandler(this.TSMI_EditRecord_Click);
            this.TSMI_EditRecord.EnabledChanged += new System.EventHandler(this.TSMI_EditRecord_EnabledChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(212, 6);
            // 
            // TSMI_DeleteRecord
            // 
            this.TSMI_DeleteRecord.Enabled = false;
            this.TSMI_DeleteRecord.Image = global::CourseWork.Properties.Resources.database_delete;
            this.TSMI_DeleteRecord.Name = "TSMI_DeleteRecord";
            this.TSMI_DeleteRecord.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.TSMI_DeleteRecord.Size = new System.Drawing.Size(215, 22);
            this.TSMI_DeleteRecord.Text = "Удалить запись";
            this.TSMI_DeleteRecord.Click += new System.EventHandler(this.TSMI_DeleteRecord_Click);
            this.TSMI_DeleteRecord.EnabledChanged += new System.EventHandler(this.TSMI_DeleteRecord_EnabledChanged);
            // 
            // TSMI_DeleteAllRecords
            // 
            this.TSMI_DeleteAllRecords.Enabled = false;
            this.TSMI_DeleteAllRecords.Image = global::CourseWork.Properties.Resources.all_delete;
            this.TSMI_DeleteAllRecords.Name = "TSMI_DeleteAllRecords";
            this.TSMI_DeleteAllRecords.Size = new System.Drawing.Size(215, 22);
            this.TSMI_DeleteAllRecords.Text = "Удалить все записи";
            this.TSMI_DeleteAllRecords.Click += new System.EventHandler(this.TSMI_DeleteAllRecords_Click);
            // 
            // TSMI_Search
            // 
            this.TSMI_Search.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_FastSearch,
            this.TSMI_FullSearch});
            this.TSMI_Search.Enabled = false;
            this.TSMI_Search.Name = "TSMI_Search";
            this.TSMI_Search.Size = new System.Drawing.Size(54, 20);
            this.TSMI_Search.Text = "Поиск";
            this.TSMI_Search.EnabledChanged += new System.EventHandler(this.TSMI_Search_EnabledChanged);
            // 
            // TSMI_FastSearch
            // 
            this.TSMI_FastSearch.Enabled = false;
            this.TSMI_FastSearch.Image = global::CourseWork.Properties.Resources.magnifier;
            this.TSMI_FastSearch.Name = "TSMI_FastSearch";
            this.TSMI_FastSearch.Size = new System.Drawing.Size(179, 22);
            this.TSMI_FastSearch.Text = "Быстрый поиск . . .";
            this.TSMI_FastSearch.Click += new System.EventHandler(this.TSMI_FastSearch_Click);
            // 
            // TSMI_FullSearch
            // 
            this.TSMI_FullSearch.Enabled = false;
            this.TSMI_FullSearch.Image = global::CourseWork.Properties.Resources.magnifier_zoom_in;
            this.TSMI_FullSearch.Name = "TSMI_FullSearch";
            this.TSMI_FullSearch.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.TSMI_FullSearch.Size = new System.Drawing.Size(179, 22);
            this.TSMI_FullSearch.Text = "Поиск . . .";
            this.TSMI_FullSearch.Click += new System.EventHandler(this.TSMI_FullSearch_Click);
            // 
            // TSMI_Additionally
            // 
            this.TSMI_Additionally.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_Tree,
            this.TSMI_MaxElement,
            this.TSMI_SumAll});
            this.TSMI_Additionally.Enabled = false;
            this.TSMI_Additionally.Name = "TSMI_Additionally";
            this.TSMI_Additionally.Size = new System.Drawing.Size(109, 20);
            this.TSMI_Additionally.Text = "Дополнительно";
            // 
            // TSMI_Tree
            // 
            this.TSMI_Tree.Enabled = false;
            this.TSMI_Tree.Image = global::CourseWork.Properties.Resources.arrow_branch;
            this.TSMI_Tree.Name = "TSMI_Tree";
            this.TSMI_Tree.Size = new System.Drawing.Size(344, 22);
            this.TSMI_Tree.Text = "Отобразить бинарное дерево поиска";
            this.TSMI_Tree.Click += new System.EventHandler(this.TSMI_Tree_Click);
            this.TSMI_Tree.EnabledChanged += new System.EventHandler(this.TSMI_Tree_EnabledChanged);
            // 
            // TSMI_MaxElement
            // 
            this.TSMI_MaxElement.Enabled = false;
            this.TSMI_MaxElement.Image = global::CourseWork.Properties.Resources.arrow_up;
            this.TSMI_MaxElement.Name = "TSMI_MaxElement";
            this.TSMI_MaxElement.Size = new System.Drawing.Size(344, 22);
            this.TSMI_MaxElement.Text = "Найти элемент с максимальной урожайностью";
            this.TSMI_MaxElement.Click += new System.EventHandler(this.TSMI_MaxElement_Click);
            this.TSMI_MaxElement.EnabledChanged += new System.EventHandler(this.TSMI_MaxElement_EnabledChanged);
            // 
            // TSMI_SumAll
            // 
            this.TSMI_SumAll.Enabled = false;
            this.TSMI_SumAll.Image = global::CourseWork.Properties.Resources.sum;
            this.TSMI_SumAll.Name = "TSMI_SumAll";
            this.TSMI_SumAll.Size = new System.Drawing.Size(344, 22);
            this.TSMI_SumAll.Text = "Найти сумму урожайностей всех овощей";
            this.TSMI_SumAll.Click += new System.EventHandler(this.TSMI_SumAll_Click);
            this.TSMI_SumAll.EnabledChanged += new System.EventHandler(this.TSMI_SumAll_EnabledChanged);
            // 
            // TSMI_Helping
            // 
            this.TSMI_Helping.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_Help,
            this.TSMI_AboutProgram,
            this.TSMI_AboutAuthor});
            this.TSMI_Helping.Name = "TSMI_Helping";
            this.TSMI_Helping.Size = new System.Drawing.Size(67, 20);
            this.TSMI_Helping.Text = "Помощь";
            // 
            // TSMI_Help
            // 
            this.TSMI_Help.Image = global::CourseWork.Properties.Resources.help;
            this.TSMI_Help.Name = "TSMI_Help";
            this.TSMI_Help.Size = new System.Drawing.Size(194, 22);
            this.TSMI_Help.Text = "Справка                   F1";
            this.TSMI_Help.Click += new System.EventHandler(this.TSMI_Help_Click);
            // 
            // TSMI_AboutProgram
            // 
            this.TSMI_AboutProgram.Image = global::CourseWork.Properties.Resources.logo;
            this.TSMI_AboutProgram.Name = "TSMI_AboutProgram";
            this.TSMI_AboutProgram.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F1)));
            this.TSMI_AboutProgram.Size = new System.Drawing.Size(194, 22);
            this.TSMI_AboutProgram.Text = "О программе";
            this.TSMI_AboutProgram.Click += new System.EventHandler(this.TSMI_AboutProgram_Click);
            // 
            // TSMI_AboutAuthor
            // 
            this.TSMI_AboutAuthor.Image = global::CourseWork.Properties.Resources.user;
            this.TSMI_AboutAuthor.Name = "TSMI_AboutAuthor";
            this.TSMI_AboutAuthor.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.TSMI_AboutAuthor.Size = new System.Drawing.Size(194, 22);
            this.TSMI_AboutAuthor.Text = "Об авторе";
            this.TSMI_AboutAuthor.Click += new System.EventHandler(this.TSMI_AboutAuthor_Click);
            // 
            // TS_Main
            // 
            this.TS_Main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TS_Main.Dock = System.Windows.Forms.DockStyle.None;
            this.TS_Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSB_New,
            this.TSB_Open,
            this.TSB_Save,
            this.TSB_CloseFile,
            this.TSB_Exit,
            this.toolStripSeparator1,
            this.TSB_AddRecord,
            this.TSB_EditRecord,
            this.TSB_DeleteRecord,
            this.TSB_DeleteAllRecords,
            this.toolStripSeparator4,
            this.TSB_FullSearch,
            this.toolStripSeparator2,
            this.TSB_Tree,
            this.TSB_MaxElement,
            this.TSB_SumAll,
            this.toolStripSeparator6,
            this.TSB_Help,
            this.TSB_AboutProgram,
            this.TSB_AboutAuthor});
            this.TS_Main.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.TS_Main.Location = new System.Drawing.Point(0, 24);
            this.TS_Main.Name = "TS_Main";
            this.TS_Main.Size = new System.Drawing.Size(393, 23);
            this.TS_Main.TabIndex = 1;
            this.TS_Main.Text = "Панель инструментов";
            // 
            // TSB_New
            // 
            this.TSB_New.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_New.Image = global::CourseWork.Properties.Resources.page;
            this.TSB_New.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_New.Name = "TSB_New";
            this.TSB_New.Size = new System.Drawing.Size(23, 20);
            this.TSB_New.Text = "Новый";
            this.TSB_New.Click += new System.EventHandler(this.TSB_New_Click);
            // 
            // TSB_Open
            // 
            this.TSB_Open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_Open.Image = global::CourseWork.Properties.Resources.book_open;
            this.TSB_Open.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_Open.Name = "TSB_Open";
            this.TSB_Open.Size = new System.Drawing.Size(23, 20);
            this.TSB_Open.Text = "Открыть";
            this.TSB_Open.Click += new System.EventHandler(this.TSB_Open_Click);
            // 
            // TSB_Save
            // 
            this.TSB_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_Save.Enabled = false;
            this.TSB_Save.Image = global::CourseWork.Properties.Resources.page_save;
            this.TSB_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_Save.Name = "TSB_Save";
            this.TSB_Save.Size = new System.Drawing.Size(23, 20);
            this.TSB_Save.Text = "Сохранить";
            this.TSB_Save.Click += new System.EventHandler(this.TSB_Save_Click);
            // 
            // TSB_CloseFile
            // 
            this.TSB_CloseFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_CloseFile.Enabled = false;
            this.TSB_CloseFile.Image = global::CourseWork.Properties.Resources.page_red;
            this.TSB_CloseFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_CloseFile.Name = "TSB_CloseFile";
            this.TSB_CloseFile.Size = new System.Drawing.Size(23, 20);
            this.TSB_CloseFile.Text = "Закрыть";
            this.TSB_CloseFile.Click += new System.EventHandler(this.TSB_CloseFile_Click);
            // 
            // TSB_Exit
            // 
            this.TSB_Exit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_Exit.Image = global::CourseWork.Properties.Resources.cancel;
            this.TSB_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_Exit.Name = "TSB_Exit";
            this.TSB_Exit.Size = new System.Drawing.Size(23, 20);
            this.TSB_Exit.Text = "Выход";
            this.TSB_Exit.Click += new System.EventHandler(this.TSB_Exit_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // TSB_AddRecord
            // 
            this.TSB_AddRecord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_AddRecord.Enabled = false;
            this.TSB_AddRecord.Image = global::CourseWork.Properties.Resources.database_add1;
            this.TSB_AddRecord.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_AddRecord.Name = "TSB_AddRecord";
            this.TSB_AddRecord.Size = new System.Drawing.Size(23, 20);
            this.TSB_AddRecord.Text = "Добавить записи";
            this.TSB_AddRecord.Click += new System.EventHandler(this.TSB_AddRecord_Click);
            // 
            // TSB_EditRecord
            // 
            this.TSB_EditRecord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_EditRecord.Enabled = false;
            this.TSB_EditRecord.Image = global::CourseWork.Properties.Resources.database_edit;
            this.TSB_EditRecord.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_EditRecord.Name = "TSB_EditRecord";
            this.TSB_EditRecord.Size = new System.Drawing.Size(23, 20);
            this.TSB_EditRecord.Text = "Изменить запись";
            this.TSB_EditRecord.Click += new System.EventHandler(this.TSB_EditRecord_Click);
            // 
            // TSB_DeleteRecord
            // 
            this.TSB_DeleteRecord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_DeleteRecord.Enabled = false;
            this.TSB_DeleteRecord.Image = global::CourseWork.Properties.Resources.database_delete;
            this.TSB_DeleteRecord.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_DeleteRecord.Name = "TSB_DeleteRecord";
            this.TSB_DeleteRecord.Size = new System.Drawing.Size(23, 20);
            this.TSB_DeleteRecord.Text = "Удалить запись";
            this.TSB_DeleteRecord.Click += new System.EventHandler(this.TSB_DeleteRecord_Click);
            // 
            // TSB_DeleteAllRecords
            // 
            this.TSB_DeleteAllRecords.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_DeleteAllRecords.Enabled = false;
            this.TSB_DeleteAllRecords.Image = global::CourseWork.Properties.Resources.all_delete;
            this.TSB_DeleteAllRecords.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_DeleteAllRecords.Name = "TSB_DeleteAllRecords";
            this.TSB_DeleteAllRecords.Size = new System.Drawing.Size(23, 20);
            this.TSB_DeleteAllRecords.Text = "Удалить все записи";
            this.TSB_DeleteAllRecords.Click += new System.EventHandler(this.TSB_DeleteAllRecords_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 23);
            // 
            // TSB_FullSearch
            // 
            this.TSB_FullSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_FullSearch.Enabled = false;
            this.TSB_FullSearch.Image = global::CourseWork.Properties.Resources.magnifier_zoom_in;
            this.TSB_FullSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_FullSearch.Name = "TSB_FullSearch";
            this.TSB_FullSearch.Size = new System.Drawing.Size(23, 20);
            this.TSB_FullSearch.Text = "Поиск . . .";
            this.TSB_FullSearch.Click += new System.EventHandler(this.TSB_FullSearch_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // TSB_Tree
            // 
            this.TSB_Tree.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_Tree.Enabled = false;
            this.TSB_Tree.Image = global::CourseWork.Properties.Resources.arrow_branch;
            this.TSB_Tree.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_Tree.Name = "TSB_Tree";
            this.TSB_Tree.Size = new System.Drawing.Size(23, 20);
            this.TSB_Tree.Text = "Отобразить бинарное дерево";
            this.TSB_Tree.Click += new System.EventHandler(this.TSB_Tree_Click);
            // 
            // TSB_MaxElement
            // 
            this.TSB_MaxElement.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_MaxElement.Enabled = false;
            this.TSB_MaxElement.Image = global::CourseWork.Properties.Resources.arrow_up;
            this.TSB_MaxElement.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_MaxElement.Name = "TSB_MaxElement";
            this.TSB_MaxElement.Size = new System.Drawing.Size(23, 20);
            this.TSB_MaxElement.Text = "Найти элемент с максимальной урожайностью";
            this.TSB_MaxElement.Click += new System.EventHandler(this.TSB_MaxElement_Click);
            // 
            // TSB_SumAll
            // 
            this.TSB_SumAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_SumAll.Enabled = false;
            this.TSB_SumAll.Image = global::CourseWork.Properties.Resources.sum;
            this.TSB_SumAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_SumAll.Name = "TSB_SumAll";
            this.TSB_SumAll.Size = new System.Drawing.Size(23, 20);
            this.TSB_SumAll.Text = "Найти сумму урожайностей всех овощей";
            this.TSB_SumAll.Click += new System.EventHandler(this.TSB_SumAll_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 23);
            // 
            // TSB_Help
            // 
            this.TSB_Help.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_Help.Image = global::CourseWork.Properties.Resources.help;
            this.TSB_Help.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_Help.Name = "TSB_Help";
            this.TSB_Help.Size = new System.Drawing.Size(23, 20);
            this.TSB_Help.Text = "Справка";
            this.TSB_Help.Click += new System.EventHandler(this.TSB_Help_Click);
            // 
            // TSB_AboutProgram
            // 
            this.TSB_AboutProgram.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_AboutProgram.Image = global::CourseWork.Properties.Resources.logo;
            this.TSB_AboutProgram.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_AboutProgram.Name = "TSB_AboutProgram";
            this.TSB_AboutProgram.Size = new System.Drawing.Size(23, 20);
            this.TSB_AboutProgram.Text = "О программе";
            this.TSB_AboutProgram.Click += new System.EventHandler(this.TSB_AboutProgram_Click);
            // 
            // TSB_AboutAuthor
            // 
            this.TSB_AboutAuthor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_AboutAuthor.Image = global::CourseWork.Properties.Resources.user;
            this.TSB_AboutAuthor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_AboutAuthor.Name = "TSB_AboutAuthor";
            this.TSB_AboutAuthor.Size = new System.Drawing.Size(23, 20);
            this.TSB_AboutAuthor.Text = "Об авторе";
            this.TSB_AboutAuthor.Click += new System.EventHandler(this.TSB_AboutAuthor_Click);
            // 
            // DGV_Main
            // 
            this.DGV_Main.AllowUserToAddRows = false;
            this.DGV_Main.AllowUserToDeleteRows = false;
            this.DGV_Main.AllowUserToResizeColumns = false;
            this.DGV_Main.AllowUserToResizeRows = false;
            this.DGV_Main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGV_Main.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGV_Main.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DGV_Main.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV_Main.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV_Main.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Main.EnableHeadersVisualStyles = false;
            this.DGV_Main.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.HP_FormMain.SetHelpKeyword(this.DGV_Main, "WorkWithData.htm");
            this.HP_FormMain.SetHelpNavigator(this.DGV_Main, System.Windows.Forms.HelpNavigator.Topic);
            this.HP_FormMain.SetHelpString(this.DGV_Main, "");
            this.DGV_Main.Location = new System.Drawing.Point(0, 50);
            this.DGV_Main.MultiSelect = false;
            this.DGV_Main.Name = "DGV_Main";
            this.DGV_Main.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV_Main.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGV_Main.RowHeadersVisible = false;
            this.DGV_Main.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGV_Main.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HP_FormMain.SetShowHelp(this.DGV_Main, true);
            this.DGV_Main.Size = new System.Drawing.Size(667, 318);
            this.DGV_Main.TabIndex = 2;
            this.DGV_Main.Visible = false;
            this.DGV_Main.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_Main_CellEnter);
            this.DGV_Main.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_Main_CellLeave);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "xml";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Файлы XML (*.xml)|*.xml|Текстовые файлы (*.txt)|*.txt";
            this.openFileDialog1.Title = "Открыть";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xml";
            this.saveFileDialog1.Filter = "Файлы XML (*.xml)|*.xml|Текстовые файлы (*.txt)|*.txt";
            this.saveFileDialog1.Title = "Сохранить";
            // 
            // TS_FastSearch
            // 
            this.TS_FastSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TS_FastSearch.Dock = System.Windows.Forms.DockStyle.None;
            this.TS_FastSearch.Enabled = false;
            this.TS_FastSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.TSTB_FastSearch,
            this.TSB_FastSearch});
            this.TS_FastSearch.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.TS_FastSearch.Location = new System.Drawing.Point(393, 24);
            this.TS_FastSearch.Name = "TS_FastSearch";
            this.TS_FastSearch.Size = new System.Drawing.Size(273, 23);
            this.TS_FastSearch.TabIndex = 3;
            this.TS_FastSearch.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.toolStripLabel1.Size = new System.Drawing.Size(103, 20);
            this.toolStripLabel1.Text = "Быстрый поиск:";
            // 
            // TSTB_FastSearch
            // 
            this.TSTB_FastSearch.Name = "TSTB_FastSearch";
            this.TSTB_FastSearch.Size = new System.Drawing.Size(144, 23);
            this.TSTB_FastSearch.ToolTipText = "Ключ для поиска \"Сорт овоща\".";
            this.TSTB_FastSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TSTB_FastSearch_KeyDown);
            // 
            // TSB_FastSearch
            // 
            this.TSB_FastSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TSB_FastSearch.Image = global::CourseWork.Properties.Resources.magnifier;
            this.TSB_FastSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_FastSearch.Name = "TSB_FastSearch";
            this.TSB_FastSearch.Size = new System.Drawing.Size(23, 20);
            this.TSB_FastSearch.Text = "Быстрый поиск";
            this.TSB_FastSearch.Click += new System.EventHandler(this.TSB_FastSearch_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.ClientSize = new System.Drawing.Size(667, 368);
            this.Controls.Add(this.TS_FastSearch);
            this.Controls.Add(this.DGV_Main);
            this.Controls.Add(this.TS_Main);
            this.Controls.Add(this.MS_Main);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HP_FormMain.SetHelpKeyword(this, "");
            this.HP_FormMain.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.TableOfContents);
            this.HP_FormMain.SetHelpString(this, "");
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.MS_Main;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormMain";
            this.HP_FormMain.SetShowHelp(this, true);
            this.Text = "Памятка дачнику-овощеводу";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.MS_Main.ResumeLayout(false);
            this.MS_Main.PerformLayout();
            this.TS_Main.ResumeLayout(false);
            this.TS_Main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Main)).EndInit();
            this.TS_FastSearch.ResumeLayout(false);
            this.TS_FastSearch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MS_Main;
        private System.Windows.Forms.ToolStripMenuItem TSMI_File;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Open;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Save;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Exit;
        private System.Windows.Forms.ToolStrip TS_Main;
        private System.Windows.Forms.ToolStripMenuItem TSMI_New;
        private System.Windows.Forms.ToolStripButton TSB_New;
        private System.Windows.Forms.ToolStripButton TSB_Open;
        private System.Windows.Forms.ToolStripButton TSB_Save;
        private System.Windows.Forms.ToolStripButton TSB_Exit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Helping;
        private System.Windows.Forms.ToolStripMenuItem TSMI_AboutProgram;
        private System.Windows.Forms.ToolStripMenuItem TSMI_AboutAuthor;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Edit;
        private System.Windows.Forms.ToolStripMenuItem TSMI_AddRecord;
        private System.Windows.Forms.ToolStripMenuItem TSMI_EditRecord;
        private System.Windows.Forms.ToolStripMenuItem TSMI_DeleteRecord;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Search;
        private System.Windows.Forms.ToolStripMenuItem TSMI_CloseFile;
        private System.Windows.Forms.ToolStripButton TSB_CloseFile;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton TSB_AboutProgram;
        private System.Windows.Forms.ToolStripButton TSB_AboutAuthor;
        private System.Windows.Forms.ToolStripButton TSB_AddRecord;
        private System.Windows.Forms.ToolStripButton TSB_EditRecord;
        private System.Windows.Forms.ToolStripButton TSB_DeleteRecord;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem TSMI_DeleteAllRecords;
        private System.Windows.Forms.ToolStripButton TSB_DeleteAllRecords;
        public System.Windows.Forms.DataGridView DGV_Main;
        private System.Windows.Forms.ToolStrip TS_FastSearch;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox TSTB_FastSearch;
        private System.Windows.Forms.ToolStripMenuItem TSMI_FastSearch;
        private System.Windows.Forms.ToolStripMenuItem TSMI_FullSearch;
        private System.Windows.Forms.ToolStripButton TSB_FastSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton TSB_FullSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Additionally;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Tree;
        private System.Windows.Forms.ToolStripButton TSB_Tree;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem TSMI_MaxElement;
        private System.Windows.Forms.ToolStripMenuItem TSMI_SumAll;
        private System.Windows.Forms.ToolStripButton TSB_MaxElement;
        private System.Windows.Forms.ToolStripButton TSB_SumAll;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Help;
        private System.Windows.Forms.ToolStripButton TSB_Help;
        public System.Windows.Forms.HelpProvider HP_FormMain;



    }
}