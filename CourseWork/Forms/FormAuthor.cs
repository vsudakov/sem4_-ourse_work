﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;

namespace CourseWork.Forms
{
    /// <summary>
    /// Форма "Об авторе".
    /// </summary>
    public partial class FormAuthor : Form
    {
        /// <summary>
        /// Объект для воспроизведения аудио.
        /// </summary>
        MediaPlayer player = new MediaPlayer();

        /// <summary>
        /// Воспроизводится ли в этот момент аудио?
        /// </summary>
        bool isPlayed;

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public FormAuthor()
        {
            InitializeComponent();

            // Плавное появление окна:
            Opacity = 0;
            Timer timer = new Timer();
            timer.Tick += new EventHandler((sender, e) =>
            {
                if ((Opacity += 0.05d) == 1) timer.Stop();
            });
            timer.Interval = 100;
            timer.Start();

            // Воспроизведение музыкального файла:
            string filePath = Application.StartupPath.Remove(Application.StartupPath.Length - 10) + @"\Sounds\sound2.mp3";
            player.Open(new Uri(filePath, UriKind.Absolute));
            player.Play();
            isPlayed = true;
        }

        private void LL_Email_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Clipboard.SetText(@"vasiasudakov@mail.ru"); // скопировать текст в буфер обмена.
        }

        private void LL_Skype_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("skype:mr.metall1st?call");
        }

        private void B_PlayPause_Click(object sender, EventArgs e)
        {
            if (isPlayed)
            {
                player.Pause();
                isPlayed = false;
            }
            else
            {
                player.Play();
                isPlayed = true;
            }
        }

        private void This_FormClosing(object sender, FormClosingEventArgs e)
        {
            player.Stop();
        }
    }
}
