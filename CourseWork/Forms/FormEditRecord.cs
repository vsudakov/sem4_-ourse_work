﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CourseWorkLibrary;
using Vasjok.Collections.Generic;

namespace CourseWork.Forms
{
    /// <summary>
    /// Форма "Редактирование записи".
    /// </summary>
    public partial class FormEditRecord : Form
    {
        /// <summary>
        /// Ссылка на родительскую форму.
        /// </summary>
        FormMain parentForm;

        string key;

        /// <summary>
        /// Конструктор формы.
        /// </summary>
        /// <param name="parentForm">Ссылка на родительскую форму.</param>
        public FormEditRecord(FormMain parentForm)
        {
            this.parentForm = parentForm;

            InitializeComponent();

            this.HP_FormEditRecord.HelpNamespace = this.parentForm.HP_FormMain.HelpNamespace;

            // Дополнительная инициализация элементов TextBox:

            this.key = (string)this.parentForm.DGV_Main.SelectedCells[1].Value;
            Vegetable v = this.parentForm.tree[key];

            this.TB_Type.Text = (this.parentForm.DGV_Main.SelectedCells[0].Value == null) ? "" : this.parentForm.DGV_Main.SelectedCells[0].Value.ToString();
            this.TB_Sort.Text = (string)this.parentForm.DGV_Main.SelectedCells[1].Value;
            this.TB_NominalProlificness.Text = (this.parentForm.DGV_Main.SelectedCells[2].Value == null) ? "" : this.parentForm.DGV_Main.SelectedCells[2].Value.ToString();
            this.TB_PlantingDate.Text = (string)this.parentForm.DGV_Main.SelectedCells[3].Value;
            this.TB_HarvestingDate.Text = (string)this.parentForm.DGV_Main.SelectedCells[4].Value;
        }

        private void B_Save_Click(object sender, EventArgs e)
        {
            bool isWasMistakes = false;

            // Сбросить все цвета в элементах TextBox:
            this.TB_Type.BackColor = Color.White;
            this.TB_Sort.BackColor = Color.White;
            this.TB_NominalProlificness.BackColor = Color.White;
            this.TB_PlantingDate.BackColor = Color.White;
            this.TB_HarvestingDate.BackColor = Color.White;

            // Проверить все вводимые данные на корректность, и если таковые есть, изменить цвет элементов TextBox соответствующих значений:
            if (!Verify.Type(this.TB_Type.Text))
            {
                isWasMistakes = true;
                this.TB_Type.BackColor = Color.LightPink;
            }

            if (!Verify.Sort(this.TB_Sort.Text))
            {
                isWasMistakes = true;
                this.TB_Sort.BackColor = Color.LightPink;
            }

            if (!Verify.NominalProlificness(this.TB_NominalProlificness.Text))
            {
                isWasMistakes = true;
                this.TB_NominalProlificness.BackColor = Color.LightPink;
            }

            if (!Verify.PlantingDate(this.TB_PlantingDate.Text))
            {
                isWasMistakes = true;
                this.TB_PlantingDate.BackColor = Color.LightPink;
            }

            if (!Verify.HarvestingDate(this.TB_HarvestingDate.Text))
            {
                isWasMistakes = true;
                this.TB_HarvestingDate.BackColor = Color.LightPink;
            }

            if (isWasMistakes) // есть ли ошибки в введённых данных?
            {
                MessageBox.Show("Введённые данные содержат ошибки! (отмечены цветом)\nПожалуйста, исправьте их и повторите снова.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            else // если в введенных данных ошибок нет
            {
                BinaryTreeHelper.Delete(this.parentForm.tree, this.key); // удалить старый элемент из дерева

                // создать новый элемент:
                Vegetable v = new Vegetable(SaveConvert.ToTypeVegetable(TB_Type.Text), TB_Sort.Text, SaveConvert.ToPositiveDouble(TB_NominalProlificness.Text), TB_PlantingDate.Text, TB_HarvestingDate.Text);
                
                // добавить новый элемент:
                BinaryTreeHelper.Add(this.parentForm.tree, v);

                // синхронизовать данные бинарного дерева и DataGridView:
                BinaryTreeHelper.Copy(this.parentForm.tree, this.parentForm.DGV_Main);
                
                this.B_Cancel.PerformClick();
            }
        }

        private void B_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
