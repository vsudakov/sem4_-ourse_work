﻿namespace CourseWork.Forms
{
    partial class FormBinaryTree
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBinaryTree));
            this.TV_Tree = new System.Windows.Forms.TreeView();
            this.HP_FormBinaryTree = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // TV_Tree
            // 
            this.TV_Tree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TV_Tree.Location = new System.Drawing.Point(13, 13);
            this.TV_Tree.Name = "TV_Tree";
            this.TV_Tree.Size = new System.Drawing.Size(259, 288);
            this.TV_Tree.TabIndex = 0;
            // 
            // FormBinaryTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.ClientSize = new System.Drawing.Size(284, 313);
            this.Controls.Add(this.TV_Tree);
            this.HP_FormBinaryTree.SetHelpKeyword(this, "ViewBinaryTree.htm");
            this.HP_FormBinaryTree.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormBinaryTree";
            this.HP_FormBinaryTree.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Бинарное дерево поиска";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.This_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView TV_Tree;
        private System.Windows.Forms.HelpProvider HP_FormBinaryTree;
    }
}