﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CourseWorkLibrary;
using Vasjok.Collections.Generic;

namespace CourseWork.Forms
{
    /// <summary>
    /// Форма для визуализации бинарного дерева.
    /// </summary>
    public partial class FormBinaryTree : Form
    {
        /// <summary>
        /// Конструктор формы.
        /// </summary>
        /// <param name="tree">Визуализируемое бинарное дерево.</param>
        public FormBinaryTree(FormMain parentForm)
        {
            InitializeComponent();

            this.HP_FormBinaryTree.HelpNamespace = parentForm.HP_FormMain.HelpNamespace;

            // Дополнительная инициализация элемента TV_Tree (TreeView) (Визуализация):
            foreach (KeyValuePair<string, Vegetable> x in parentForm.tree)
            {
                if (TV_Tree.Nodes.Count == 0)
                {
                    TreeNode added = new TreeNode(x.Value.Sort);
                    added.Nodes.Add("null"); // right
                    added.Nodes.Add("null"); // left
                    TV_Tree.Nodes.Add(added);
                }
                else
                {
                    TreeNode tn = TV_Tree.Nodes[0];
                    while (tn.Text != "null")
                        if (x.Value.Sort.CompareTo(tn.Text) > 0)
                            tn = tn.Nodes[0];
                        else
                            tn = tn.Nodes[1];

                    tn.Text = x.Value.Sort;
                    tn.Nodes.Add("null");
                    tn.Nodes.Add("null");
                }
            }
        }

        private void This_FormClosing(object sender, FormClosingEventArgs e)
        {
            TV_Tree.Dispose();
        }
    }
}
