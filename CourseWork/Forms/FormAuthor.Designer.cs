﻿namespace CourseWork.Forms
{
    partial class FormAuthor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAuthor));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LL_Email = new System.Windows.Forms.LinkLabel();
            this.LL_Skype = new System.Windows.Forms.LinkLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.B_PlayPause = new System.Windows.Forms.Button();
            this.PB_Foto = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PB_Foto)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(225, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(207, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Студент группы ИТ-22 Судаков В.С.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(226, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "E-mail:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(225, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Skype:";
            // 
            // LL_Email
            // 
            this.LL_Email.AutoSize = true;
            this.LL_Email.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.LL_Email.Location = new System.Drawing.Point(279, 32);
            this.LL_Email.Name = "LL_Email";
            this.LL_Email.Size = new System.Drawing.Size(132, 15);
            this.LL_Email.TabIndex = 4;
            this.LL_Email.TabStop = true;
            this.LL_Email.Text = "vasiasudakov@mail.ru";
            this.toolTip1.SetToolTip(this.LL_Email, "Нажмите, чтобы скопировать в буфер обмена\r\nэлектронный адрес автора.");
            this.LL_Email.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LL_Email_LinkClicked);
            // 
            // LL_Skype
            // 
            this.LL_Skype.AutoSize = true;
            this.LL_Skype.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.LL_Skype.Location = new System.Drawing.Point(279, 54);
            this.LL_Skype.Name = "LL_Skype";
            this.LL_Skype.Size = new System.Drawing.Size(75, 15);
            this.LL_Skype.TabIndex = 5;
            this.LL_Skype.TabStop = true;
            this.LL_Skype.Text = "mr.metall1st";
            this.toolTip1.SetToolTip(this.LL_Skype, "Нажмите, чтобы позвонить автору по Skype.");
            this.LL_Skype.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LL_Skype_LinkClicked);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(303, 310);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 49);
            this.button1.TabIndex = 6;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // B_PlayPause
            // 
            this.B_PlayPause.BackColor = System.Drawing.Color.White;
            this.B_PlayPause.Image = global::CourseWork.Properties.Resources.play_and_pause;
            this.B_PlayPause.Location = new System.Drawing.Point(310, 165);
            this.B_PlayPause.Name = "B_PlayPause";
            this.B_PlayPause.Size = new System.Drawing.Size(75, 49);
            this.B_PlayPause.TabIndex = 6;
            this.toolTip1.SetToolTip(this.B_PlayPause, "Play/Pause");
            this.B_PlayPause.UseVisualStyleBackColor = false;
            this.B_PlayPause.Click += new System.EventHandler(this.B_PlayPause_Click);
            // 
            // PB_Foto
            // 
            this.PB_Foto.Dock = System.Windows.Forms.DockStyle.Left;
            this.PB_Foto.Image = global::CourseWork.Properties.Resources.foto;
            this.PB_Foto.Location = new System.Drawing.Point(0, 0);
            this.PB_Foto.Name = "PB_Foto";
            this.PB_Foto.Size = new System.Drawing.Size(219, 330);
            this.PB_Foto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PB_Foto.TabIndex = 0;
            this.PB_Foto.TabStop = false;
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // FormAuthor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(463, 330);
            this.Controls.Add(this.B_PlayPause);
            this.Controls.Add(this.LL_Skype);
            this.Controls.Add(this.LL_Email);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PB_Foto);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAuthor";
            this.Text = "Об авторе";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.This_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.PB_Foto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PB_Foto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel LL_Email;
        private System.Windows.Forms.LinkLabel LL_Skype;
        private System.Windows.Forms.Button B_PlayPause;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}