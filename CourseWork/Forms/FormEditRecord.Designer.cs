﻿namespace CourseWork.Forms
{
    partial class FormEditRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditRecord));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TB_Type = new System.Windows.Forms.TextBox();
            this.TB_Sort = new System.Windows.Forms.TextBox();
            this.TB_NominalProlificness = new System.Windows.Forms.TextBox();
            this.TB_PlantingDate = new System.Windows.Forms.TextBox();
            this.TB_HarvestingDate = new System.Windows.Forms.TextBox();
            this.B_Save = new System.Windows.Forms.Button();
            this.B_Cancel = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.HP_FormEditRecord = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(42, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Вид овоща:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(-1, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 30);
            this.label2.TabIndex = 2;
            this.label2.Text = "Номинальная уро-\r\n  жайность (кг/м2):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(75, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Сорт:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(14, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 30);
            this.label4.TabIndex = 4;
            this.label4.Text = "Рекомендуемая\r\n    дата посадки:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(20, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 30);
            this.label5.TabIndex = 5;
            this.label5.Text = "Ркомендуемая\r\n    дата уборки:";
            // 
            // TB_Type
            // 
            this.TB_Type.Location = new System.Drawing.Point(120, 10);
            this.TB_Type.Name = "TB_Type";
            this.TB_Type.Size = new System.Drawing.Size(194, 20);
            this.TB_Type.TabIndex = 6;
            this.toolTip1.SetToolTip(this.TB_Type, "Здесь может быть одно из значений:\r\nКартофель|Помидор|Огурец|Перец|Редис|Салат\r\n(" +
        "это поле можно оставить пустым)");
            // 
            // TB_Sort
            // 
            this.TB_Sort.Location = new System.Drawing.Point(119, 35);
            this.TB_Sort.Name = "TB_Sort";
            this.TB_Sort.Size = new System.Drawing.Size(194, 20);
            this.TB_Sort.TabIndex = 7;
            this.toolTip1.SetToolTip(this.TB_Sort, "Не оставляйте это поле пустым.");
            // 
            // TB_NominalProlificness
            // 
            this.TB_NominalProlificness.Location = new System.Drawing.Point(120, 64);
            this.TB_NominalProlificness.Name = "TB_NominalProlificness";
            this.TB_NominalProlificness.Size = new System.Drawing.Size(194, 20);
            this.TB_NominalProlificness.TabIndex = 8;
            this.toolTip1.SetToolTip(this.TB_NominalProlificness, "Целое число либо десятичная дробь.\r\n(это поле можно оставить пустым)");
            // 
            // TB_PlantingDate
            // 
            this.TB_PlantingDate.Location = new System.Drawing.Point(120, 109);
            this.TB_PlantingDate.Name = "TB_PlantingDate";
            this.TB_PlantingDate.Size = new System.Drawing.Size(194, 20);
            this.TB_PlantingDate.TabIndex = 9;
            this.toolTip1.SetToolTip(this.TB_PlantingDate, "(это поле можно оставить пустым)");
            // 
            // TB_HarvestingDate
            // 
            this.TB_HarvestingDate.Location = new System.Drawing.Point(119, 155);
            this.TB_HarvestingDate.Name = "TB_HarvestingDate";
            this.TB_HarvestingDate.Size = new System.Drawing.Size(194, 20);
            this.TB_HarvestingDate.TabIndex = 10;
            this.toolTip1.SetToolTip(this.TB_HarvestingDate, "(это поле можно оставить пустым)");
            // 
            // B_Save
            // 
            this.B_Save.BackColor = System.Drawing.Color.ForestGreen;
            this.B_Save.ForeColor = System.Drawing.SystemColors.Window;
            this.B_Save.Location = new System.Drawing.Point(61, 192);
            this.B_Save.Name = "B_Save";
            this.B_Save.Size = new System.Drawing.Size(113, 23);
            this.B_Save.TabIndex = 11;
            this.B_Save.Text = "Сохранить";
            this.B_Save.UseVisualStyleBackColor = false;
            this.B_Save.Click += new System.EventHandler(this.B_Save_Click);
            // 
            // B_Cancel
            // 
            this.B_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.B_Cancel.Location = new System.Drawing.Point(180, 192);
            this.B_Cancel.Name = "B_Cancel";
            this.B_Cancel.Size = new System.Drawing.Size(75, 23);
            this.B_Cancel.TabIndex = 12;
            this.B_Cancel.Text = "Отмена";
            this.B_Cancel.UseVisualStyleBackColor = true;
            this.B_Cancel.Click += new System.EventHandler(this.B_Cancel_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // FormEditRecord
            // 
            this.AcceptButton = this.B_Save;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.CancelButton = this.B_Cancel;
            this.ClientSize = new System.Drawing.Size(325, 221);
            this.Controls.Add(this.B_Cancel);
            this.Controls.Add(this.B_Save);
            this.Controls.Add(this.TB_HarvestingDate);
            this.Controls.Add(this.TB_PlantingDate);
            this.Controls.Add(this.TB_NominalProlificness);
            this.Controls.Add(this.TB_Sort);
            this.Controls.Add(this.TB_Type);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HP_FormEditRecord.SetHelpKeyword(this, "EditRecord.htm");
            this.HP_FormEditRecord.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormEditRecord";
            this.HP_FormEditRecord.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактирование";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TB_Type;
        private System.Windows.Forms.TextBox TB_Sort;
        private System.Windows.Forms.TextBox TB_NominalProlificness;
        private System.Windows.Forms.TextBox TB_PlantingDate;
        private System.Windows.Forms.TextBox TB_HarvestingDate;
        private System.Windows.Forms.Button B_Save;
        private System.Windows.Forms.Button B_Cancel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.HelpProvider HP_FormEditRecord;
    }
}