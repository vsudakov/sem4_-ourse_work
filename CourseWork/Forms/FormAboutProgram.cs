﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseWork.Forms
{
    /// <summary>
    /// Формма "О программе".
    /// </summary>
    public partial class FormAboutProgram : Form
    {
        /// <summary>
        /// Конструктор формы.
        /// </summary>
        public FormAboutProgram()
        {
            InitializeComponent();
        }

        private void B_OK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
