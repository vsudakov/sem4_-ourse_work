﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CourseWorkLibrary;
using Vasjok.Collections.Generic;

namespace CourseWork.Forms
{
    public partial class FormFind : Form
    {
        /// <summary>
        /// Ссылка на родительскую форму.
        /// </summary>
        FormMain parentForm;

        /// <summary>
        /// Конструктор формы.
        /// </summary>
        /// <param name="parentForm">Ссылка на родительскую форму.</param>
        public FormFind(FormMain parentForm)
        {
            this.parentForm = parentForm;

            InitializeComponent();

            this.HP_FormFind.HelpNamespace = this.parentForm.HP_FormMain.HelpNamespace;

            // Дополнительная инициализация DGV_Main:
            this.DGV_Search.ColumnCount = 5;
            this.DGV_Search.Columns[0].HeaderText = "Вид овоща";
            this.DGV_Search.Columns[1].HeaderText = "Сорт";
            this.DGV_Search.Columns[2].HeaderText = "Номинальная урожайность (кг/м2)";
            this.DGV_Search.Columns[3].HeaderText = "Дата посадки";
            this.DGV_Search.Columns[4].HeaderText = "Дата уборки";
        }

        private void B_Search_Click(object sender, EventArgs e)
        {
            if (BinaryTreeHelper.Search(parentForm.tree, this.TB_Key.Text, this.DGV_Search))
            {
                this.DGV_Search.Visible = true;
                this.B_Go.Visible = true;
                this.B_Cancel.Visible = true;
                this.Height = 288;
            }
            else
            {
                this.DGV_Search.Visible = false;
                this.B_Go.Visible = false;
                this.B_Cancel.Visible = false;
                this.Height = 75;
                MessageBox.Show("Поиск не дал результатов!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
        }

        private void B_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void B_Go_Click(object sender, EventArgs e)
        {
            string key = (string)this.DGV_Search.SelectedCells[1].Value;

            for(int i = 0; i < parentForm.DGV_Main.RowCount; i++)
                if ((string)parentForm.DGV_Main[1, i].Value == key)
                {
                    parentForm.DGV_Main[1, i].Selected = true;
                    break;
                }

            this.B_Cancel.PerformClick();
        }
    }
}
