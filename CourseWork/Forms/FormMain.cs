﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using CourseWorkLibrary;
using Vasjok.Collections.Generic;

namespace CourseWork.Forms
{
    public partial class FormMain : Form
    {
        /// <summary>
        /// Экземпляр бинарного дерева.
        /// </summary>
        public BinaryTree<string, Vegetable> tree = new BinaryTree<string,Vegetable>();

        /// <summary>
        /// Результат события закрытия файла.
        /// </summary>
        protected enum ResultCloseFile
        {
            Yes, No, Cancel
        }

        /// <summary>
        /// Хранит результат закрытия файла.
        /// </summary>
        protected ResultCloseFile? resultCloseFile = null;

        /// <summary>
        /// Конструктор формы.
        /// </summary>
        public FormMain()
        {
            InitializeComponent();

            this.HP_FormMain.HelpNamespace = Application.StartupPath.Remove(Application.StartupPath.Length - 10) + @"\Help\Help_v.1.0.0.chm";

            // Дополнительная инициализация DGV_Main:
            this.DGV_Main.ColumnCount = 5;
            this.DGV_Main.Columns[0].HeaderText = "Вид овоща";
            this.DGV_Main.Columns[1].HeaderText = "Сорт";
            this.DGV_Main.Columns[2].HeaderText = "Номинальная урожайность (кг/м2)";
            this.DGV_Main.Columns[3].HeaderText = "Дата посадки";
            this.DGV_Main.Columns[4].HeaderText = "Дата уборки";
        }

        #region DGV_Main (Компонент DataGridView)

        private void DGV_Main_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.DGV_Main.Rows[e.RowIndex].Selected = true;
            this.TSMI_EditRecord.Enabled = true;
            this.TSMI_DeleteRecord.Enabled = true;
        }

        private void DGV_Main_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            this.TSMI_EditRecord.Enabled = false;
            this.TSMI_DeleteRecord.Enabled = false;
        }

        #endregion

        #region TS_Main (Компонент ToolStrip)

        private void TSB_New_Click(object sender, EventArgs e)
        {
            this.TSMI_New.PerformClick();
        }

        private void TSB_Open_Click(object sender, EventArgs e)
        {
            this.TSMI_Open.PerformClick();
        }

        private void TSB_Save_Click(object sender, EventArgs e)
        {
            this.TSMI_Save.PerformClick();
        }

        private void TSB_CloseFile_Click(object sender, EventArgs e)
        {
            this.TSMI_CloseFile.PerformClick();
        }

        private void TSB_Exit_Click(object sender, EventArgs e)
        {
            this.TSMI_Exit.PerformClick();
        }

        private void TSB_AddRecord_Click(object sender, EventArgs e)
        {
            this.TSMI_AddRecord.PerformClick();
        }

        private void TSB_EditRecord_Click(object sender, EventArgs e)
        {
            this.TSMI_EditRecord.PerformClick();
        }

        private void TSB_DeleteRecord_Click(object sender, EventArgs e)
        {
            this.TSMI_DeleteRecord.PerformClick();
        }

        private void TSB_DeleteAllRecords_Click(object sender, EventArgs e)
        {
            this.TSMI_DeleteAllRecords.PerformClick();
        }

        private void TSB_FullSearch_Click(object sender, EventArgs e)
        {
            this.TSMI_FullSearch.PerformClick();
        }

        private void TSB_Tree_Click(object sender, EventArgs e)
        {
            this.TSMI_Tree.PerformClick();
        }

        private void TSB_MaxElement_Click(object sender, EventArgs e)
        {
            this.TSMI_MaxElement.PerformClick();
        }

        private void TSB_SumAll_Click(object sender, EventArgs e)
        {
            this.TSMI_SumAll.PerformClick();
        }

        private void TSB_Help_Click(object sender, EventArgs e)
        {
            this.TSMI_Help.PerformClick();
        }

        private void TSB_AboutProgram_Click(object sender, EventArgs e)
        {
            this.TSMI_AboutProgram.PerformClick();
        }

        private void TSB_AboutAuthor_Click(object sender, EventArgs e)
        {
            this.TSMI_AboutAuthor.PerformClick();
        }

        #endregion

        #region TS_FastSearch (Компонент ToolStrip)

        private void TSB_FastSearch_Click(object sender, EventArgs e)
        {
            if (tree.ContainsKey(TSTB_FastSearch.Text))
            {
                for (int i = 0; i < DGV_Main.RowCount; i++)
                    if ((string)DGV_Main[1, i].Value == TSTB_FastSearch.Text)
                    {
                        DGV_Main[1, i].Selected = true;
                        break;
                    }
            }
            else
            {
                MessageBox.Show("Поиск не дал результатов!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
        }

        private void TSTB_FastSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                TSB_FastSearch.PerformClick();
        }

        #endregion

        #region MS_Main (Компонент MenuStrip) Вкладка "Файл"

        private void TSMI_New_Click(object sender, EventArgs e)
        {
            this.TSMI_CloseFile.PerformClick();

            if(this.resultCloseFile != ResultCloseFile.Cancel)
            {
                this.DGV_Main.Visible = true;

                this.TSMI_Save.Enabled = true;
                this.TSMI_CloseFile.Enabled = true;
                this.TSMI_Edit.Enabled = true;
                this.TSMI_Search.Enabled = true;
                this.TSMI_Additionally.Enabled = true;
                this.TSMI_Tree.Enabled = true;
                this.TSMI_MaxElement.Enabled = true;
                this.TSMI_SumAll.Enabled = true;

                this.Text += " - [Новый]";
            }
        }

        private void TSMI_Open_Click(object sender, EventArgs e)
        {
            this.TSMI_CloseFile.PerformClick();

            if (this.resultCloseFile != ResultCloseFile.Cancel)
            {
                openFileDialog1.InitialDirectory = Application.StartupPath.Remove(Application.StartupPath.Length - 10) + @"\DataDefault";

                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fileName = openFileDialog1.FileName;
                    string extension = fileName.Remove(0, fileName.Length - 3); // расширение выбранного файла
                    bool isWasMistakes = false;

                    if (extension == "xml") this.tree = Read.FromXml(fileName, out isWasMistakes);
                    else if (extension == "txt") this.tree = Read.FromTxt(fileName, out isWasMistakes);

                    if (isWasMistakes)
                        MessageBox.Show("При чтении файла были ошибки!", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    this.TSMI_New.PerformClick();
                    BinaryTreeHelper.Copy(tree, this.DGV_Main);

                    //поменять путь к файлу в заголовке:
                    string replacing = Regex.Match(this.Text, @" - \[.*\]$").ToString();
                    if (replacing != "")
                        this.Text = this.Text.Replace(replacing, " - [" + fileName + "]");
                    else
                        this.Text += " - [" + fileName + "]";
                }
            }
        }

        private void TSMI_Save_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = Application.StartupPath.Remove(Application.StartupPath.Length - 10) + @"\DataDefault";

            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string fileName = saveFileDialog1.FileName;
                string extension = fileName.Remove(0, fileName.Length - 3); // получить расширение выбранного файла

                if (extension == "xml") Write.ToXml(this.tree, fileName);
                else if (extension == "txt") Write.ToTxt(this.tree, fileName);

                //поменять путь к файлу в заголовке:
                string replacing = Regex.Match(this.Text, @" - \[.*\]$").ToString();
                if (replacing != "")
                    this.Text = this.Text.Replace(replacing, " - [" + fileName + "]");
                else
                    this.Text += " - [" + fileName + "]";
            }
        }

        private void TSMI_Save_EnabledChanged(object sender, EventArgs e)
        {
            this.TSB_Save.Enabled = this.TSMI_Save.Enabled;
        }

        private void TSMI_CloseFile_Click(object sender, EventArgs e)
        {
            if (this.DGV_Main.RowCount > 0) // если таблица не пуста, сохранить её?
            {
                DialogResult dr = MessageBox.Show("Таблица не пуста. Если закрыть её, то эти данные будут потеряны.\nСохранить данные перед продолжением?", "Предупреждение", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                switch (dr)
                {
                    case System.Windows.Forms.DialogResult.Yes:
                        this.resultCloseFile = ResultCloseFile.Yes;
                        this.TSMI_Save.PerformClick();
                        break;
                    case System.Windows.Forms.DialogResult.No:
                        this.resultCloseFile = ResultCloseFile.No;
                        break;
                    case System.Windows.Forms.DialogResult.Cancel:
                        this.resultCloseFile = ResultCloseFile.Cancel;
                        return;
                }
            }
            else
            {
                this.resultCloseFile = null;
            }

            if(tree != null) this.tree.Clear();

            this.DGV_Main.Visible = false;
            this.DGV_Main.RowCount = 0;

            this.TSMI_Save.Enabled = false;
            this.TSMI_CloseFile.Enabled = false;
            this.TSMI_Edit.Enabled = false;
            this.TSMI_Search.Enabled = false;
            this.TSMI_Additionally.Enabled = false;
            this.TSMI_Tree.Enabled = false;
            this.TSMI_MaxElement.Enabled = false;
            this.TSMI_SumAll.Enabled = false;

            // очистить путь к файлу в загловке:
            string replacing = Regex.Match(this.Text, @" - \[.*\]$").ToString();
            if (replacing != "")
                this.Text = this.Text.Replace(replacing, "");
        }

        private void TSMI_CloseFile_EnabledChanged(object sender, EventArgs e)
        {
            this.TSB_CloseFile.Enabled = this.TSMI_CloseFile.Enabled;
        }

        private void TSMI_Exit_Click(object sender, EventArgs e)
        {
            this.TSMI_CloseFile.PerformClick();

            if (this.resultCloseFile != ResultCloseFile.Cancel)
            {
                this.Close();
            }
        }

        #endregion

        #region MS_Main (Компонент MenuStrip) Вкладка "Правка"

        private void TSMI_AddRecord_Click(object sender, EventArgs e)
        {
            (new FormAddRecord(this)).Show();
        }

        private void TSMI_EditRecord_Click(object sender, EventArgs e)
        {
            (new FormEditRecord(this)).Show();
        }

        private void TSMI_EditRecord_EnabledChanged(object sender, EventArgs e)
        {
            TSB_EditRecord.Enabled = TSMI_EditRecord.Enabled;
        }

        private void TSMI_DeleteRecord_Click(object sender, EventArgs e)
        {
            // получить ключ выделенной записи:
            string key = (string)DGV_Main.SelectedCells[1].Value;

            if (DialogResult.Yes == MessageBox.Show("Вы действительно хотите удалить запись?", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1))
            {
                BinaryTreeHelper.Delete(tree, key);
                BinaryTreeHelper.Copy(tree, DGV_Main);
            }
        }

        private void TSMI_DeleteRecord_EnabledChanged(object sender, EventArgs e)
        {
            TSB_DeleteRecord.Enabled = TSMI_DeleteRecord.Enabled;
        }

        private void TSMI_DeleteAllRecords_Click(object sender, EventArgs e)
        {
            if (tree.Count != 0)
            {
                if (DialogResult.No == MessageBox.Show("Вы действительно хотите удалить все записи?", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2))
                    return;
            }

            tree.Clear();
            BinaryTreeHelper.Copy(tree, this.DGV_Main);
        }

        private void TSMI_Edit_EnabledChanged(object sender, EventArgs e)
        {
            TSB_AddRecord.Enabled = TSMI_Edit.Enabled;
            TSB_DeleteAllRecords.Enabled = TSMI_Edit.Enabled;
            TSMI_AddRecord.Enabled = TSMI_Edit.Enabled;
            TSMI_DeleteAllRecords.Enabled = TSMI_Edit.Enabled;
        }

        #endregion

        #region MS_Main (Компонент MenuStrip) Вкладка "Поиск"

        private void TSMI_FastSearch_Click(object sender, EventArgs e)
        {
            this.TSTB_FastSearch.Focus();
        }

        private void TSMI_FullSearch_Click(object sender, EventArgs e)
        {
            (new FormFind(this)).Show();
        }

        private void TSMI_Search_EnabledChanged(object sender, EventArgs e)
        {
            TS_FastSearch.Enabled = TSMI_Search.Enabled;
            TSB_FullSearch.Enabled = TSMI_Search.Enabled;
            TSMI_FastSearch.Enabled = TSMI_Search.Enabled;
            TSMI_FullSearch.Enabled = TSMI_Search.Enabled;
        }

        #endregion

        #region MS_Main (Компонент MenuStrip) Вкладка "Дополнительно"

        private void TSMI_Tree_Click(object sender, EventArgs e)
        {
            (new FormBinaryTree(this)).Show();
        }

        private void TSMI_Tree_EnabledChanged(object sender, EventArgs e)
        {
            TSB_Tree.Enabled = TSMI_Tree.Enabled;
        }

        private void TSMI_MaxElement_Click(object sender, EventArgs e)
        {
            if (tree.Count != 0) // если дерево непустое
            {
                Vegetable max = Vegetable.MaxObject(tree);
                for (int i = 0; i < DGV_Main.RowCount; i++)
                    if ((string)DGV_Main[1, i].Value == max.Sort)
                    {
                        DGV_Main[1, i].Selected = true;
                        break;
                    }
            }
            else                // если пустое
            {
                MessageBox.Show("Таблица пуста.\nМаксимальный элемент не найден!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
        }

        private void TSMI_MaxElement_EnabledChanged(object sender, EventArgs e)
        {
            this.TSB_MaxElement.Enabled = TSMI_MaxElement.Enabled;
        }

        private void TSMI_SumAll_Click(object sender, EventArgs e)
        {
            double sum = 0;
            foreach (KeyValuePair<string, Vegetable> x in tree)
                if (x.Value.NominalProlificness.HasValue)
                    sum += (double)x.Value.NominalProlificness;

            MessageBox.Show(String.Format("Сумма урожайностей всех овощей равна:\n{0:F2}", sum), "Результат", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }

        private void TSMI_SumAll_EnabledChanged(object sender, EventArgs e)
        {
            this.TSB_SumAll.Enabled = TSMI_SumAll.Enabled;
        }

        #endregion

        #region MS_Main (Компонент MenuStrip) Вкладка "Помощь"

        private void TSMI_Help_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(this.HP_FormMain.HelpNamespace);
        }

        private void TSMI_AboutProgram_Click(object sender, EventArgs e)
        {
            (new FormAboutProgram()).Show();
        }

        private void TSMI_AboutAuthor_Click(object sender, EventArgs e)
        {
            (new FormAuthor()).Show();
        }

        #endregion

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.TSMI_CloseFile.PerformClick();
        }
    }
}
