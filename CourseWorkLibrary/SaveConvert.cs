﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWorkLibrary
{
    /// <summary>
    /// Безопасно преобразует значения одного типа в другой.
    /// </summary>
    public static class SaveConvert
    {
        /// <summary>
        /// string -> TypeVegetable? (при невозможности конвертации возвращает null).
        /// </summary>
        /// <param name="value">Преобразуемое значение.</param>
        public static TypeVegetable? ToTypeVegetable(string value)
        {
            TypeVegetable? res = null;
            try
            {
                res = (TypeVegetable?)Enum.Parse(typeof(TypeVegetable), value);
            }
            catch
            {

            }
            return res;
        }

        /// <summary>
        /// string -> положительное Double? (при невозможности конвертации возвращает null).
        /// </summary>
        /// <param name="value">Преобразуемое значение.</param>
        /// <returns></returns>
        public static double? ToPositiveDouble(string value)
        {
            double? res = null;
            try
            {
                res = Convert.ToDouble(value);
                if (res <= 0) return null;
            }
            catch
            {

            }
            return res;
        }
    }
}
