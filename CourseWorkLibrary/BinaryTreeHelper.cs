﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

using Vasjok.Collections.Generic;

namespace CourseWorkLibrary
{
    /// <summary>
    /// Реализует взаимодействие форм с типом BinaryTree.
    /// </summary>
    public static class BinaryTreeHelper
    {
        /// <summary>
        /// Копирует элементы из экземпляра BinaryTree&lt;string, Vegetable> в элемент DataGridView.
        /// </summary>
        /// <param name="tree">Источник.</param>
        /// <param name="DGV">Место назначения.</param>
        public static void Copy(ICollection<KeyValuePair<string, Vegetable>> tree, DataGridView DGV)
        {
            DGV.RowCount = tree.Count;

            int i = 0;
            foreach (KeyValuePair<string, Vegetable> x in tree)
            {
                DGV[0, i].Value = x.Value.Type;
                DGV[1, i].Value = x.Value.Sort;
                DGV[2, i].Value = String.Format("{0:F2}", x.Value.NominalProlificness);
                DGV[3, i].Value = x.Value.PlantingDate;
                DGV[4, i].Value = x.Value.HarvestingDate;
                i++;
            }

            if(DGV.SortedColumn != null)
                DGV.Sort(DGV.SortedColumn, System.ComponentModel.ListSortDirection.Ascending);
        }

        /// <summary>
        /// Ищет элемент по ключу key в бинрном дереве tree и возвращает результат поиска в DGV.
        /// </summary>
        /// <param name="tree">Место поиска.</param>
        /// <param name="key">Ключ.</param>
        /// <param name="DGV">Место для вывода результата.</param>
        /// <returns></returns>
        public static bool Search(BinaryTree<string, Vegetable> tree, string key, DataGridView DGV)
        {
            List<KeyValuePair<string, Vegetable>> L = new List<KeyValuePair<string, Vegetable>>();

            KeyValuePair<string, Vegetable> v;

            while (tree.ContainsKey(key))
            {
                v = new KeyValuePair<string, Vegetable>(tree[key].Sort, tree[key]);
                L.Add(v);
                key += " ";
            }

            BinaryTreeHelper.Copy(L, DGV);

            if (L.Count == 0) return false;
            return true;
        }

        /// <summary>
        /// Удаляет элемент с ключом key из дерева tree.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="key"></param>
        public static void Delete(BinaryTree<string, Vegetable> tree, string key)
        {
            tree.Remove(key);
            key += " ";
            while (tree.ContainsKey(key))
            {
                Vegetable temp = tree[key]--;
                tree.Remove(key);
                tree.Add(new KeyValuePair<string, Vegetable>(temp.Sort, temp));
                key += " ";
            }
        }

        /// <summary>
        /// Добавляет элемент v в бинарное дерево tree.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="v"></param>
        public static void Add(BinaryTree<string, Vegetable> tree, Vegetable v)
        {
            while (true)
            {
                try
                {
                    tree.Add(v.Sort, v);
                    break;
                }
                catch(TheNodeAlreadyExists)
                {
                    v++;
                }
            }
        }
    }
}
