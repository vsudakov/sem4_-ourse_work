﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWorkLibrary
{
    /// <summary>
    /// Вид овоща.
    /// </summary>
    public enum TypeVegetable
    {
        Картофель, Помидор, Огурец, Перец, Редис, Салат
    }

    /// <summary>
    /// Класс, моделирующий объект в природе "Овощь" определенного вида и сорта.
    /// </summary>
    public class Vegetable : IComparable
    {
        #region FIELDS

        /// <summary>
        /// Вид овоща.
        /// </summary>
        TypeVegetable? type;

        /// <summary>
        /// Сорт овоща. (Является КЛЮЧом при использовании бинарного дерева BinaryTree.)
        /// </summary>
        string sort;

        /// <summary>
        /// Номинальная урожайность.
        /// </summary>
        double? nominalProlificness;

        /// <summary>
        /// Рекомендуемая дата посадки.
        /// </summary>
        string plantingDate;

        /// <summary>
        /// Рекомендуемая дата уборки.
        /// </summary>
        string harvestingDate;

        #endregion

        /// <summary>
        /// Конструктор с параметрами.
        /// </summary>
        /// <param name="type">Вид овоща.</param>
        /// <param name="sort">Сорт овоща.</param>
        /// <param name="nominalProlificness">Номинальная урожайность.</param>
        /// <param name="plantingDate">Рекомендуемая дата посадки.</param>
        /// <param name="harvestingDate">Рекомендуемая дата уборки.</param>
        public Vegetable(TypeVegetable? type, string sort, double? nominalProlificness, string plantingDate, string harvestingDate)
        {
            this.Type = type;
            this.Sort = sort;
            this.NominalProlificness = nominalProlificness;
            this.PlantingDate = plantingDate;
            this.HarvestingDate = harvestingDate;
        }

        #region PROPERTIES

        /// <summary>
        /// Свойство для доступа к полю type.
        /// </summary>
        public TypeVegetable? Type
        {
            get { return type; }
            protected set {type = value;}
        }

        /// <summary>
        /// Свойство для доступа к полю sort.
        /// </summary>
        public string Sort
        {
            get { return sort; }
            protected set { sort = value; }
        }

        /// <summary>
        /// Свойство для доступа к полю nominalProlificness.
        /// </summary>
        public double? NominalProlificness
        {
            get { return nominalProlificness; }
            protected set { nominalProlificness = value; }
        }

        /// <summary>
        /// Свойство для доступа к полю plantingDate.
        /// </summary>
        public string PlantingDate
        {
            get { return plantingDate; }
            protected set { plantingDate = value; }
        }

        /// <summary>
        /// Свойство для доступа к полю harvestingDate.
        /// </summary>
        public string HarvestingDate
        {
            get { return harvestingDate; }
            protected set { harvestingDate = value; }
        }

        #endregion

        #region Перегрузки стандартных операторов

        /// <summary>
        /// Перегрузка операции "больше". Сравнивет два экземпляра по полю sort.
        /// </summary>
        public static bool operator >(Vegetable v1, Vegetable v2)
        {
            if (String.Compare(v1.sort, v2.sort, true) > 0)
                return true;
            return false;
        }

        /// <summary>
        /// Перегрузка операции "меньше". Сравнивет два экземпляра по полю sort.
        /// </summary>
        public static bool operator <(Vegetable v1, Vegetable v2)
        {
            if (String.Compare(v1.sort, v2.sort, true) < 0)
                return true;
            return false;
        }

        /// <summary>
        /// Пергерузка операции инкремента. Прибавляет к строке sort пробел. Тем самым изменяет
        /// ключ экземпляра этого класса, если экземпляр с таким ключом уже существует в бинарном дереве.
        /// </summary>
        public static Vegetable operator ++(Vegetable v)
        {
            v.Sort += " ";
            return v;
        }

        /// <summary>
        /// Пергерузка операции декремента. Удаляет все пробелы в конце строки sort.
        /// </summary>
        public static Vegetable operator --(Vegetable v)
        {
            if (v.Sort.Length > 0)
                if (v.Sort[v.Sort.Length - 1] == ' ')
                    v.Sort = v.Sort.Remove(v.Sort.Length - 1);

            return v;
        }

        /// <summary>
        /// Перегрузка операции сложения. Слаживает поля NominalProlificness объектов v1 и v2, все остальные поля берутся из v1.
        /// </summary>
        public static Vegetable operator +(Vegetable v1, Vegetable v2)
        {
            return new Vegetable(v1.Type, v1.Sort, v1.NominalProlificness + v2.NominalProlificness, v1.PlantingDate, v1.HarvestingDate);
        }

        #endregion

        /// <summary>
        /// Удаляет все пробелы в конце строки sort.
        /// </summary>
        /// <returns></returns>
        public Vegetable RemoveAllSpaces()
        {
            int num = 0;
            for (int i = this.Sort.Length - 1; i >= 0; i--)
                if (this.Sort[i] == ' ')
                    num++;
                else
                    break;

            if (num > 0)
                this.Sort = this.Sort.Remove(this.Sort.Length - num);

            return this;
        }

        /// <summary>
        /// Определяет максимальный объект в коллекции collection (по полю NominalProlificness)
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static Vegetable MaxObject(ICollection<KeyValuePair<string, Vegetable>> collection)
        {
            Vegetable max = null;
            bool isInitialize = false;

            foreach (KeyValuePair<string, Vegetable> x in collection)
            {
                max = x.Value;
                isInitialize = true;
                break;
            }

            if(isInitialize)
                foreach (KeyValuePair<string, Vegetable> x in collection)
                {
                    if (x.Value.NominalProlificness > max.NominalProlificness)
                        max = x.Value;
                }

            return (isInitialize) ? max : null;
        }

        #region Реализация IComparable

        /// <summary>
        /// Сравнивает текущий объект с объектом этого же типа obj по полю sort.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            Vegetable temp = (Vegetable)obj;
            if (this > temp)
                return 1;
            if (this < temp)
                return -1;
            return 0;
        }

        #endregion
    }
}
