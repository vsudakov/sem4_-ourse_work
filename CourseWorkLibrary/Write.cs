﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

using Vasjok.Collections.Generic;

namespace CourseWorkLibrary
{
    /// <summary>
    /// Записывает даннные из BinaryTree в файл.
    /// </summary>
    public static class Write
    {
        /// <summary>
        /// Записывает данные в XML-документ.
        /// </summary>
        /// <param name="tree">Источник данных.</param>
        /// <param name="fileName">Путь для создаваемого XML-документа.</param>
        public static void ToXml(BinaryTree<string,Vegetable> tree, string fileName)
        {
            XmlWriterSettings settings = new XmlWriterSettings(); // задать форматирование для выходного XML-документа
            settings.CloseOutput = true;
            settings.Indent = true;
            settings.IndentChars = "    "; 
            settings.NewLineChars = "\n";

            XmlWriter output = XmlWriter.Create(fileName, settings);

            output.WriteStartElement("head");

            int i = 1;
            foreach (KeyValuePair<string, Vegetable> x in tree)
            {
                Vegetable v = x.Value;
                v.RemoveAllSpaces();

                output.WriteStartElement("vegetable");
                output.WriteAttributeString("id", (i++).ToString());

                output.WriteElementString("type", String.Format("{0}",v.Type));
                output.WriteElementString("sort", v.Sort);
                output.WriteElementString("nominal_prolificness", String.Format("{0}",v.NominalProlificness));
                output.WriteElementString("planting_date", v.PlantingDate);
                output.WriteElementString("harvesting_date", v.HarvestingDate);

                output.WriteEndElement();
            }

            output.WriteEndElement();

            output.Flush(); // сбросить буфферизованные данные

            output.Close(); // закрыть файл, с которым связан output
        }

        /// <summary>
        /// Записывает данные в TXT файл.
        /// </summary>
        /// <param name="tree">Источник данных.</param>
        /// <param name="fileName">Путь для создаваемого TXT файла.</param>
        public static void ToTxt(BinaryTree<string, Vegetable> tree, string fileName)
        {
            StreamWriter F = new StreamWriter(fileName, false); // создать новый файл, либо перезапизать его, если такой уже существует

            int i = 0;
            foreach(KeyValuePair<string, Vegetable> x in tree)
            {
                Vegetable v = x.Value;
                v.RemoveAllSpaces();
                F.Write(String.Format("{0}",v.Type) + ";" + v.Sort + ";" + String.Format("{0}",v.NominalProlificness) + ";" + v.PlantingDate + ";" + v.HarvestingDate);
                if (++i != tree.Count) F.WriteLine();
            }

            F.Close();
        }
    }
}
