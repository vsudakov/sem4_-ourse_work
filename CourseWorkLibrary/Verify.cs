﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using Vasjok.Collections.Generic;

namespace CourseWorkLibrary
{
    /// <summary>
    /// Проверяет корректность данных, которые может содержать класс Vegetable.
    /// </summary>
    public static class Verify
    {
        public static bool Type(string value)
        {
            if (value == null || value == "") return true;

            if (Regex.IsMatch(value, @"\A(Картофель){1}\Z|\A(Помидор){1}\Z|\A(Огурец){1}\Z|\A(Перец){1}\Z|\A(Редис){1}\Z|\A(Салат){1}\Z")) return true;
            else return false;
        }

        public static bool Sort(string value)
        {
            if (value == null || value == "") return false;

            return true;
        }

        public static bool NominalProlificness(string value)
        {
            if (value == null || value == "") return true;

            if (Regex.IsMatch(value, @"(^[+]?\d*,?\d*[1-9]+\d*$)|(^[+]?[1-9]+\d*,\d*$)")) return true;
            else return false;
        }

        public static bool PlantingDate(string value)
        {
            if (value == null || value == "") return true;
            
            return true;
        }

        public static bool HarvestingDate(string value)
        {
            if (value == null || value == "") return true;

            return true;
        }
    }
}
