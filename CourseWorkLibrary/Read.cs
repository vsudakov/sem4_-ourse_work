﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

using Vasjok.Collections.Generic;

namespace CourseWorkLibrary
{
    /// <summary>
    /// Считывает данные из файла и создаёт из этих данных объект BinaryTree&lt;string, Vegetable>.
    /// </summary>
    public static class Read
    {
        /// <summary>
        /// Считывает данные из файла формата XML.
        /// </summary>
        /// <param name="fileName">Путь к файлу с данными.</param>
        /// <param name="isWasMistakes">Переменная, указывающая, были ли ошибки при чтении из файла.</param>
        /// <returns></returns>
        public static BinaryTree<string, Vegetable> FromXml(string fileName, out bool isWasMistakes)
        {
            isWasMistakes = false;

            BinaryTree<string, Vegetable> res = new BinaryTree<string, Vegetable>();
            
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);
 
            foreach (XmlNode v in xmlDoc.DocumentElement.ChildNodes)
            {
                string value;

                value = v.ChildNodes[1].InnerText;
                string sort;
                if (Verify.Sort(value)) sort = value;
                else { isWasMistakes = true; continue; } // если ключевое слово проверку не прошло, то сразу перейти к следующей итерации

                value = v.ChildNodes[0].InnerText;
                TypeVegetable? type = null;
                if(Verify.Type(value)) type = SaveConvert.ToTypeVegetable(value);
                else isWasMistakes = true;

                value = v.ChildNodes[2].InnerText;
                double? nominalProlificness = null;
                if(Verify.NominalProlificness(value)) nominalProlificness = SaveConvert.ToPositiveDouble(value);
                else isWasMistakes = true;

                value = v.ChildNodes[3].InnerText;
                string plantingDate = null;
                if (Verify.PlantingDate(value)) plantingDate = value;
                else isWasMistakes = true;

                value = v.ChildNodes[4].InnerText;
                string harvestingDate = null;
                if (Verify.HarvestingDate(value)) harvestingDate = value;
                else isWasMistakes = true;

                Vegetable vegetable = new Vegetable(type, sort, nominalProlificness, plantingDate, harvestingDate);

                BinaryTreeHelper.Add(res, vegetable);
            }
            
            return res;
        }

        /// <summary>
        /// Считывает данные из файла формата TXT.
        /// </summary>
        /// <param name="fileName">Путь к файлу с данными.</param>
        /// <param name="isWasMistakes">Переменная, указывающая, были ли ошибки при чтении из файла.</param>
        /// <returns></returns>
        public static BinaryTree<string, Vegetable> FromTxt(string fileName, out bool isWasMistakes)
        {
            isWasMistakes = false;

            BinaryTree<string, Vegetable> res = new BinaryTree<string, Vegetable>();

            StreamReader F = new StreamReader(fileName);

            while(!F.EndOfStream)
            {
                string buf = F.ReadLine();
                string[] data = buf.Split(';');

                string value;

                value = data[1];
                string sort;
                if (Verify.Sort(value)) sort = value;
                else { isWasMistakes = true; continue; } // если ключевое слово проверку не прошло, то сразу перейти к следующей итерации

                value = data[0];
                TypeVegetable? type = null;
                if (Verify.Type(value)) type = SaveConvert.ToTypeVegetable(value);
                else isWasMistakes = true;

                value = data[2];
                double? nominalProlificness = null;
                if (Verify.NominalProlificness(value)) nominalProlificness = SaveConvert.ToPositiveDouble(value);
                else isWasMistakes = true;

                value = data[3];
                string plantingDate = null;
                if (Verify.PlantingDate(value)) plantingDate = value;
                else isWasMistakes = true;

                value = data[4];
                string harvestingDate = null;
                if (Verify.HarvestingDate(value)) harvestingDate = value;
                else isWasMistakes = true;

                Vegetable vegetable = new Vegetable(type, sort, nominalProlificness, plantingDate, harvestingDate);

                BinaryTreeHelper.Add(res, vegetable);
            }

            F.Close();

            return res;
        }
    }
}
