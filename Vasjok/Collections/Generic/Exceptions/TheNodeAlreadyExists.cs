﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vasjok.Collections.Generic
{
    /// <summary>
    /// Исключение, которое выбрасывается, когда узел с определённым ключом уже существует.
    /// </summary>
    public class TheNodeAlreadyExists : Exception
    {
        public TheNodeAlreadyExists()
            : base("Узел с таким ключом в бинарном дереве уже существует!")
        { }

        public TheNodeAlreadyExists(string message)
            : base(message)
        { }

        public TheNodeAlreadyExists(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
