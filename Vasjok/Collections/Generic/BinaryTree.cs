﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vasjok.Collections.Generic
{
    /// <summary>
    /// Класс моделирует бинарное дерево поиска.
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class BinaryTree<TKey, TValue> : IDictionary<TKey, TValue>
        where TKey : IComparable
        where TValue : IComparable
    {
        /// <summary>
        /// Вложенный класс, моделирующий узел бинарного дерева.
        /// </summary>
        protected class Node : IComparable
        {
            Node left;
            Node right;
            TKey key;
            TValue value;

            public Node(TKey key, TValue value)
            {
                this.Key = key;
                this.Value = value;
            }

            public Node Left
            {
                get { return left; }
                set { left = value; }
            }

            public Node Right
            {
                get { return right; }
                set { right = value; }
            }

            public TKey Key
            {
                get { return key; }
                set { key = value; }
            }

            public TValue Value
            {
                get { return this.value; }
                set { this.value = value; }
            }

            #region IComparable Members

            public int CompareTo(object obj)
            {
                Node temp = (Node)obj;
                return this.Key.CompareTo(temp.Key);
            }

            #endregion
        }

        #region FIELDS

        /// <summary>
        /// Корень бинарного дерева.
        /// </summary>
        protected Node root;

        /// <summary>
        /// Число узлов.
        /// </summary>
        protected int count;

        #endregion

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public BinaryTree() { }

        #region PROPERTIES

        /// <summary>
        /// Свойство для доступа к полю root.
        /// </summary>
        protected Node Root
        {
            get { return root; }
            set { root = value; }
        }

        /// <summary>
        /// Пустое ли дерево?
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                if (this.Root == null) return true;
                return false;
            }
        }

        #endregion

        #region IDictionary<TKey, TValue> Members

        /// <summary>
        /// Добавляет новый узел с ключом key и с данными value.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <exception cref="System.CourseWork.TheNodeAlreadyExists">Выбрасывается, когда добавляемый узел с ключом key, уже существует в дереве.</exception>
        /// <exception cref="System.ArgumentNullException">Выбрасывается, когда хотя бы один из переадваемых аргументов равен null.(Только для ссылочных типов)</exception>
        public void Add(TKey key, TValue value)
        {
            if ((!key.GetType().IsValueType) ? (key == null) : false || (!value.GetType().IsValueType) ? (value == null) : false)
                throw new ArgumentNullException();

            Node newNode = new Node(key, value);

            if (this.IsEmpty) // дерево пустое?
                this.Root = newNode;
            else
            {
                Node current = this.Root, parent = null;

                while (current != null)
                {
                    parent = current;
                    if (newNode.CompareTo(current) > 0) current = current.Right;
                    else if (newNode.CompareTo(current) < 0) current = current.Left;
                    else throw new TheNodeAlreadyExists(); // если узел с ключом data.sort уже существует
                }

                if (newNode.CompareTo(parent) > 0) parent.Right = newNode;
                else parent.Left = newNode;
            }

            this.Count++;
        }

        /// <summary>
        /// Содержит ли дерево узел с ключом key?
        /// </summary>
        /// <param name="key">Искомый ключ.</param>
        /// <returns></returns>
        public bool ContainsKey(TKey key)
        {
            Node current = this.Root;

            while (current != null)
                if (current.Key.CompareTo(key) == 0)
                    return true;
                else
                {
                    if (current.Key.CompareTo(key) > 0) current = current.Left;
                    else current = current.Right;
                }

            return false;
        }

        /// <summary>
        /// Возвращает коллекцию, содержащую все ключи узлов.
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                List<TKey> res = new List<TKey>(this.Count);
                foreach (KeyValuePair<TKey, TValue> x in this)
                    res.Add(x.Key);
                return res;
            }
        }

        /// <summary>
        /// Удаляет элемент по ключу key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Возвращает: true, если удаление было произведено;
        ///                      false, если нет.</returns>
        public bool Remove(TKey key)
        {
            Node Del = this.Root, Prev = null;

            // Поиск удаляемого узла и его предыдущего элемента:
            bool isLeftBranch = false; // указывает, является ли удаляемый элемент левым поддеревом.
            bool isFound = false;

            while (Del != null && !isFound)
            {
                if (key.CompareTo(Del.Key) == 0) isFound = true;
                else if (key.CompareTo(Del.Key) < 0)
                {
                    isLeftBranch = true;
                    Prev = Del;
                    Del = Del.Left;
                }
                else
                {
                    isLeftBranch = false;
                    Prev = Del;
                    Del = Del.Right;
                }
            }

            // Если узел не найден, то удаления не происходит:
            if (!isFound) return false;

            this.Count--; // элемент в дереве содержится и удаление будет произведено => уменьшить количество элементов

            if ((Del == this.Root) && Del.Left == null && Del.Right == null)
            {
                this.Root = null;
                return true;
            }

            if ((Del == this.Root) && Del.Left == null && Del.Right != null)
            {
                this.Root = Del.Right;
                Del = null;
                return true;
            }

            if ((Del == this.Root) && Del.Left != null && Del.Right == null)
            {
                this.Root = Del.Left;
                Del = null;
                return true;
            }

            if ((Del == this.Root) && Del.Left != null && Del.Right != null)
            {
                Node tempPrev = null;
                Node temp = Del.Right;

                while (temp.Left != null)
                {
                    tempPrev = temp;
                    temp = temp.Left;
                }

                if (temp.Right != null && tempPrev != null) tempPrev.Left = temp.Right;
                else if (tempPrev != null && temp.Right == null) tempPrev.Left = null;

                temp.Left = Del.Left;

                if (Del.Right == temp) temp.Right = temp.Right;
                else temp.Right = Del.Right;

                this.Root = temp;
                Del = null;
                return true;
            }

            if (Del.Left == null && Del.Right == null)
            {
                if (isLeftBranch) Prev.Left = null;
                else if (!isLeftBranch) Prev.Right = null;
                Del = null;
                return true;
            }

            if (Del.Right == null && Del.Left != null)
            {
                if (isLeftBranch) Prev.Left = Del.Left;
                else if (!isLeftBranch) Prev.Right = Del.Left;
                Del = null;
                return true;
            }

            if (Del.Right != null && Del.Left == null)
            {
                if (isLeftBranch) Prev.Left = Del.Right;
                else if (!isLeftBranch) Prev.Right = Del.Right;
                Del = null;
                return true;
            }

            if (Del.Right != null && Del.Left != null)
            {
                Node tempPrev = null;
                Node temp = Del.Right;

                while (temp.Left != null)
                {
                    tempPrev = temp;
                    temp = temp.Left;
                }

                if (temp.Right != null && tempPrev != null) tempPrev.Left = temp.Right;
                else if (tempPrev != null && temp.Right == null) tempPrev.Left = null;

                temp.Left = Del.Left;

                if (Del.Right == temp) temp.Right = temp.Right;
                else temp.Right = Del.Right;

                if (isLeftBranch) Prev.Left = temp;
                else if (!isLeftBranch) Prev.Right = temp;

                Del = null;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Получает значение по ключу, если это возможно.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            value = default(TValue); // значение NULL для ссылочных типов и ноль для типов значения.

            Node current = this.Root;

            while (current != null)
                if (key.CompareTo(current.Key) == 0)
                {
                    value = current.Value;
                    return true;
                }
                else if (key.CompareTo(current.Key) > 0)
                    current = current.Right;
                else
                    current = current.Left;

            return false;
        }

        /// <summary>
        /// Возвращает коллекцию, содержащую все значения узлов.
        /// </summary>
        public ICollection<TValue> Values
        {
            get
            {
                List<TValue> res = new List<TValue>(this.Count);
                foreach (KeyValuePair<TKey, TValue> x in this)
                    res.Add(x.Value);
                return res;
            }
        }

        /// <summary>
        /// Предоставляет доступ к значению по ключу key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue this[TKey key]
        {
            get
            {
                TValue res;
                this.TryGetValue(key, out res);
                return res;
            }
            set
            {
                if ((value.GetType().IsValueType) ? false : (value == null))
                {
                    // ничего не присваивать.
                }
                else
                {
                    Node current = this.Root;
                    while (current != null)
                        if (key.CompareTo(current.Key) == 0)
                        {
                            current.Value = value;
                            return;
                        }
                        else if (key.CompareTo(current.Key) > 0)
                            current = current.Right;
                        else
                            current = current.Left;
                }
            }
        }

        #endregion

        #region ICollection<KeyValuePair<TKey, TValue>> Members

        /// <summary>
        /// Добавляет новый узел с ключом key и с данными value, которые содержит item.
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="System.CourseWork.TheNodeAlreadyExists">Выбрасывается, когда добавляемый узел с ключом key, уже существует в дереве.</exception>
        /// <exception cref="System.ArgumentNullException">Выбрасывается, когда хотя бы один из переадваемых аргументов равен null.(Только для ссылочных типов)</exception>
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            if ((!item.Key.GetType().IsValueType) ? (item.Key == null) : false || (!item.Value.GetType().IsValueType) ? (item.Value == null) : false)
                throw new ArgumentNullException();

            this.Add(item.Key, item.Value);
        }

        /// <summary>
        /// Удаляет все элементы.
        /// </summary>
        public void Clear()
        {
            this.Root = null;
            this.Count = 0;
        }

        /// <summary>
        /// Определяет, содержит ли дерево указанное значение.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return this.ContainsKey(item.Key);
        }

        /// <summary>
        /// Копирует все элементы BinaryTree в массив Array, начиная с указанного индекса Array.
        /// </summary>
        /// <param name="array">Массив, в который копируются элементы.</param>
        /// <param name="arrayIndex">Индекс в массиве array, с которого начинается запись.</param>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            int i = arrayIndex;
            foreach (KeyValuePair<TKey, TValue> x in this)
            {
                array[i] = x;
                i++;
            }
        }

        /// <summary>
        /// Свойство для доступа к полю count.
        /// </summary>
        public int Count
        {
            get { return count; }
            protected set
            {
                if (value >= 0) count = value;
                else count = 0;
            }
        }

        /// <summary>
        /// Доступно ли бмнарное дерево только для чтения?
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Удаляет элемент по ключу item.Key.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Возвращает: true, если удаление было произведено;
        ///                      false, если нет.</returns>
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return this.Remove(item.Key);
        }

        #endregion

        #region IEnumerable<KeyValuePair<TKey, TValue>> Members

        /// <summary>
        /// Обходит узлы дерева.
        /// (Способ обхода: в ширину, с использованием очереди.)
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            Node current = this.Root;
            Queue<Node> queue = new Queue<Node>();
            while (current != null)
            {
                yield return new KeyValuePair<TKey, TValue>(current.Key, current.Value);
                if (current.Left != null) queue.Enqueue(current.Left);
                if (current.Right != null) queue.Enqueue(current.Right);
                if (queue.Count != 0) current = queue.Dequeue();
                else current = null;
            }
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Обходит узлы дерева.
        /// (Способ обхода: в ширину, с использованием очереди.)
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

    }
}
